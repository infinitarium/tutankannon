﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;

namespace Test1.Utilidades
{
    /// <summary>
    /// Esta clase controlará las entradas del usuario,
    /// enviará las respuestas correctas detectando si es una 
    /// consola o un pc
    /// </summary>
    public static class InputManager
    {
        
        /// <summary>
        /// detecta una entrada y responde con una de las entradas posibles del usuario.
        /// La idea es mejorar y sumar los parámetros del jostick también.
        /// </summary>
        /// <param name="teclaPresionada"></param>
        /// <returns></returns>
        public static bool DetectarInput(Entradas entrada) 
        {
            KeyboardState teclaPresionada = Keyboard.GetState();
            switch (entrada)
            {
                case Entradas.Nada:
                    break;
                case Entradas.Arriba:
                    if (teclaPresionada.IsKeyDown(Keys.Up))
                        return true;
                    break;
                case Entradas.Abajo:
                    if (teclaPresionada.IsKeyDown(Keys.Down))
                        return true;
                    break;
                case Entradas.Izquierda:
                    if (teclaPresionada.IsKeyDown(Keys.Left))
                        return true;
                    break;
                case Entradas.Derecha:
                    if (teclaPresionada.IsKeyDown(Keys.Right))
                        return true;
                    break;
                case Entradas.Estacionar:
                    if (teclaPresionada.IsKeyDown(Keys.LeftShift)|| teclaPresionada.IsKeyDown(Keys.RightShift))
                        return true;
                    break;
                case Entradas.Disparar:
                    if (teclaPresionada.IsKeyDown(Keys.Q))
                        return true;
                    break;
                default:
                    break;
            }
           
            return false;
        }
    }
}
