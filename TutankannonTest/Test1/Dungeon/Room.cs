﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using GeneracionProcedural;
using Microsoft.Xna.Framework.Input;
using Test1.Balas;
using Test1.Entidades;
using Test1.Content;
using Vector2 = Microsoft.Xna.Framework.Vector2;
using Test1.Utilidades;

namespace Test1
{
    public class Room
    {
        #region Campos y Propiedades
        
        public int ROOM_HEIGHT = 9, ROOM_WIDTH = 14, WALL = 1, TILE_SIZE = 16;
        private int[] _esquinaSuperior;
        private int[] _esquinaInferior;
        public int TileSize { get; set; }
        public TipoBloque[,] _bloquesRoom;
        private Texture2D[,] _roomTexture;
        private List<Puerta> _puertas;

        public TipoBloque[,] BloquesRoom 
        {
            get 
            {
                return _bloquesRoom;
            }
        }

        private Entidad[,] _mapaEntidades;

        public Entidad[,] MapaEntidades
        {
            get { return _mapaEntidades; }
        }
        
        private int[] _posPlayer;
        public int[] PosPlayer
        {
            get { return _posPlayer; }
        }
        public int[] EsquinaInferior
        {
            get
            {
                return _esquinaInferior;
            }
        }
        public int[] EsquinaSuperior
        {
            get
            {
                return _esquinaSuperior;
            }
        }
        
        public Vector2 CentroRoom 
        {
            get 
            {
                return new Vector2((_esquinaSuperior[0] + TileSize * _bloquesRoom.GetLength(0)) / 2, (_esquinaSuperior[1] + TileSize * _bloquesRoom.GetLength(1)) / 2); 
            }
        }
        
        #endregion

        #region Constructor e Inicializador
        public Room()
        {
            Init();
        }

        public Room(int coordX, int coorY, Roomie room) : this()
        {
            //Se establece el alto y el ancho de la habitacion
            _esquinaSuperior[0] = coordX;
            _esquinaSuperior[1] = coorY;
            _esquinaInferior[0] = coordX + ROOM_HEIGHT;
            _esquinaInferior[1] = coorY + ROOM_WIDTH;
            GenerarRoom(room);
        }

        public void Initialize(int coordX, int coorY, Roomie room)
        {
            //Se establece el alto y el ancho de la habitacion
            _esquinaSuperior[0] = coordX;
            _esquinaSuperior[1] = coorY;
            _esquinaInferior[0] = coordX + ROOM_HEIGHT;
            _esquinaInferior[1] = coorY + ROOM_WIDTH;
            GenerarRoom(room);
            PosicionarPlayer();
        }

        private void Init()
        {
            _esquinaInferior = new int[2];
            _esquinaSuperior = new int[2];
            _bloquesRoom = new TipoBloque[ROOM_WIDTH, ROOM_HEIGHT];
            _posPlayer = new int[4];
            _puertas = new List<Puerta>();
            TileSize = 16;
        } 
        #endregion

        #region Métodos Privados
        private void GenerarRoom(Roomie room)
        {
            _bloquesRoom = room.Room;
            //el largo de las texturas debe ser el mismo que el de los bloques
            _roomTexture = new Texture2D[_bloquesRoom.GetLength(0), _bloquesRoom.GetLength(1)];
            _mapaEntidades = new Entidad[_bloquesRoom.GetLength(0), _bloquesRoom.GetLength(1)];

            //Coloca las texturas del room
            for (int fila = 0; fila < _bloquesRoom.GetLength(0); fila++)
            {
                for (int col = 0; col < _bloquesRoom.GetLength(1); col++)
                {
                    _roomTexture[fila, col] = AssetManager.GetTexturaBloque(_bloquesRoom[fila, col]);
                    if (_bloquesRoom[fila, col] == TipoBloque.Door || _bloquesRoom[fila, col] == TipoBloque.LockedDoor || _bloquesRoom[fila, col] == TipoBloque.BossDoor)
                    {//añade la puerta
                        _puertas.Add(new Puerta()
                        {
                            PosX = fila,
                            PosY = col,
                            Tipo = _bloquesRoom[fila, col],
                            //colola la dirección a la puerta
                            Posicion = fila == 0 ? Direccion.Izquierda : fila == _bloquesRoom.GetLength(0) - 1 ? Direccion.Derecha :
                                       col == 0 ? Direccion.Arriba : col == _bloquesRoom.GetLength(1) - 1 ? Direccion.Abajo : Direccion.Estacionado
                        });
                    }
                }
            }
        }

        /// <summary>
        /// Encuentra una posición para el jugador, también retorna sus coordenadas x e y de mapa
        /// </summary>
        private void PosicionarPlayer()
        {
            //Pos hora posicionare al player en una posicion vacía del _dungeon
            for (int i = 0; i < _bloquesRoom.GetLength(0); i++)
            {
                for (int j = 0; j < _bloquesRoom.GetLength(1); j++)
                {
                    if (_bloquesRoom[i, j] == (int)TipoBloque.Void)
                    {
                        //se calculan las coordenadas en la pantalla en base a las del room
                        _posPlayer[0] = _esquinaInferior[0] + (TILE_SIZE * i);
                        _posPlayer[1] = _esquinaInferior[1] + (TILE_SIZE * j);
                        _posPlayer[2] = i;
                        _posPlayer[3] = j;
                        //colocar al player dentro del mapa de entidades
                        MapaEntidades[i, j] = Entidad.Player;
                        return;
                    }
                }
            }
        }


       
        
        #endregion

        #region Métodos Públicos
        /// <summary>
        /// Dibuja en pantalla el room actual seleccionado
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch)
        {
            Vector2 position;
            for (int fila = 0; fila < _bloquesRoom.GetLength(0); fila++)
            {
                for (int col = 0; col < _bloquesRoom.GetLength(1); col++)
                {
                    //dibujar de acuerdo al tipo de bloque                   
                    //calcula la posición en pantalla para dibujar el bloque
                    position = new Vector2(EsquinaInferior[0] + (TILE_SIZE * fila), EsquinaInferior[1] + (TILE_SIZE * col));
                    //dibuja el bloque en pantalla
                    spriteBatch.Draw
                    (
                        //depende del tipo de bloque                            
                       _roomTexture[fila, col],
                        position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f
                    );
                }
            }
        }

        /// <summary>
        /// Resuelve si la bala chocó con algo o no y los efectos en el 
        /// tablero que esto ocasiona
        /// </summary>
        /// <param name="bala"></param>
        /// <returns></returns>
        public bool BalaEstaColisionando(Bullet bala, ref List<Animacion> explosiones) 
        {
            try
            {
                int posXBala, posYBala; 
                TipoBloque bloqueBala;
                bala.BalaEstaColisionando(this, out posXBala, out posYBala, out bloqueBala);
                if (bloqueBala == TipoBloque.Void)
                    return false;
                //TODO: Hacer que en vez de desaparecer se dañe y luego destruya
                else if (bloqueBala == TipoBloque.Destructible)
                {
                    //actualizar el bloque y su posición
                    _bloquesRoom[posXBala, posYBala] = TipoBloque.Destructible2;
                    _roomTexture[posXBala, posYBala] = AssetManager.GetTexturaBloque(TipoBloque.Destructible2);
                }
                else if (bloqueBala == TipoBloque.Destructible2)
                {
                    //actualizar el bloque y su posición
                    _bloquesRoom[posXBala, posYBala] = TipoBloque.Void;
                    _roomTexture[posXBala, posYBala] = AssetManager.GetTexturaBloque(TipoBloque.Void);
                    //Colocar la tExplosion del bloque en su lugar
                    explosiones.Add(AssetManager.GetExplosion(TExplosiones.Bloque, new Vector2(EsquinaInferior[0] + (TILE_SIZE * posXBala), EsquinaInferior[1] + (TILE_SIZE * posYBala))));
                }
                return true;
            }
            catch (Exception)
            {
                //En caso que la bala salga fuera de la plataforma o quede en una siuación de "Error"
                //Se considera que no ha impactado
                return false;
            }
            
        }

        public void Update(Player player, KeyboardState teclaPresionada) 
        {
            player.Update( ref _mapaEntidades, ref _bloquesRoom, TILE_SIZE);
        }
        #endregion
    }
}
