﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneracionProcedural;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Test1.Content;
using Microsoft.Xna.Framework;
using Vector2 = Microsoft.Xna.Framework.Vector2;

namespace Test1
{
    public static class AssetManager
    {
        #region Campos y Propiedades
        public static int MAX_TEXTURAS_TIPO = 10;
        private static List<Texture2D>[] _texturas;
        private static List<Texture2D>[] _explosiones;
        //textura de cada bloque
        private static List<Texture2D>[] _texturasBloque;

        private static List<Texture2D>[] TexturasBloque
        {
            get
            {
                if (_texturasBloque == null)
                {
                    //la cantidad de textura de bloque depende los tipos que bloque que existan
                    _texturasBloque = new List<Texture2D>[Enum.GetValues(typeof(TipoBloque)).Length];
                    //inicializar cada una de las listas también
                    for (int i = 0; i < _texturasBloque.Length; i++)
                        _texturasBloque[i] = new List<Texture2D>();
                }
                
                return _texturasBloque;
            }
            set { _texturasBloque = value; }
        }
        private static List<Texture2D>[] Texturas
        {
            get
            {
                if (_texturas == null)
                {
                    //la cantidad de textura de bloque depende los tipos que bloque que existan
                    _texturas = new List<Texture2D>[Enum.GetValues(typeof(Texturas)).Length];
                    //inicializar cada una de las listas también
                    for (int i = 0; i < _texturas.Length; i++)
                        _texturas[i] = new List<Texture2D>();
                }

                return _texturas;
            }
            set { _texturasBloque = value; }
        }

        private static List<Texture2D>[] Explosiones
        {
            get
            {
                if (_explosiones == null)
                {
                    //la cantidad de textura de bloque depende los tipos que bloque que existan
                    _explosiones = new List<Texture2D>[Enum.GetValues(typeof(TExplosiones)).Length];
                    //inicializar cada una de las listas también
                    for (int i = 0; i < _explosiones.Length; i++)
                        _explosiones[i] = new List<Texture2D>();
                }

                return _explosiones;
            }
            set { _explosiones = value; }
        }     
        #endregion

        #region Métodos
        /// <summary>
        /// Carga un tile dentro del Asset Manager
        /// </summary>
        /// <param name="textura"></param>
        /// <param name="content"></param>
        /// <param name="filename"></param>
        public static void CargarTile(TipoBloque tipoBloque, ContentManager content, String filename)
        {
            //añade loa textura al listado de texturas correspondiente
            TexturasBloque[(int)tipoBloque].Add(content.Load<Texture2D>(filename));
        }

        /// <summary>
        /// Carga una textura dentro del Asset Manager
        /// </summary>
        /// <param name="textura"></param>
        /// <param name="content"></param>
        /// <param name="filename"></param>
        public static void CargarTextura(Texturas textura, ContentManager content, String filename)
        {
            //añade loa textura al listado de texturas correspondiente
            Texturas[(int)textura].Add(content.Load<Texture2D>(filename));
        }

        /// <summary>
        /// Carga en memoria todos los frames de una explosión según su tipo
        /// </summary>
        /// <param name="tExplosion">Tipo de explosión a cargar</param>
        /// <param name="content">Sirve para llamar a las carpetas</param>
        public static void CargarExplosion(TExplosiones explosion, ContentManager content) 
        {
            switch (explosion)
            {
                case TExplosiones.Bloque:
                    //añade loa textura al listado de texturas correspondiente
                    Explosiones[(int)explosion].Add(content.Load<Texture2D>("Graphics\\Rompible3"));
                    Explosiones[(int)explosion].Add(content.Load<Texture2D>("Graphics\\Rompible4"));
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Retorna la Animación de explosión lista para ser explotada ;)
        /// </summary>
        /// <param name="tExplosion"></param>
        /// <returns></returns>
        public static Animacion GetExplosion(TExplosiones tExplosion, Vector2 posicion) 
        {
            Animacion explosion = new Animacion(posicion);
            //añade los frames coorrespondiente a la explosiom
            foreach (Texture2D frame in Explosiones[(int)tExplosion])
                explosion.AddFrame(frame);
            return explosion;
        }

        /// <summary>
        /// Retorna una textura para un tipo de bloque dado
        /// </summary>
        /// <param name="textura">Bloque especificado de retorno de la textura</param>
        /// <returns>textura2D de acuerdo a un bloque</returns>
        public static Texture2D GetTexturaBloque(TipoBloque tipoBloque)
        {
            //Sino existe la textura indicada envía una excepcion
            if (TexturasBloque[(int)tipoBloque] == null)
                throw new ArgumentException(string.Format("No se han cargado texturas del tipo {0}", tipoBloque));
            //retorna una textura al azar del tipo especificado
            return TexturasBloque[(int)tipoBloque][Aleatorio.Rng.Next(0, TexturasBloque[(int)tipoBloque].Count)];
        }


        /// <summary>
        /// Retorna una textura para un tipo de bloque dado
        /// </summary>
        /// <param name="textura">Bloque especificado de retorno de la textura</param>
        /// <returns>textura2D de acuerdo a un bloque</returns>
        public static Texture2D GetTextura(Texturas textura)
        {
            //Sino existe la textura indicada envía una excepcion
            if (Texturas[(int)textura] == null)
                throw new ArgumentException(string.Format("No se han cargado texturas del tipo {0}", textura));
            //retorna una textura al azar del tipo especificado
            return Texturas[(int)textura][Aleatorio.Rng.Next(0, Texturas[(int)textura].Count)];
        } 

        #endregion
    }
}
