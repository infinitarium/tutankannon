﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneracionProcedural;

namespace Test1
{
    public class Puerta
    {
        public int PosX { get; set; }
        public int PosY { get; set; }
        public Direccion Posicion { get; set; }
        public TipoBloque Tipo { get; set; }

        public Vector2 PosicionSalida() 
        {
            switch (Posicion)
            {
                case Direccion.Arriba:
                    return new Vector2(PosX, PosY + 1);
                case Direccion.Derecha:
                    return new Vector2(PosX - 1, PosY);
                case Direccion.Abajo:
                    return new Vector2(PosX, PosY - 1);
                case Direccion.Izquierda:
                    return new Vector2(PosX + 1, PosY - 1);
                default:
                    return new Vector2(PosX, PosY);
            }
        }
    }
}
