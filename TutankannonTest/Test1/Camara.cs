﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Test1
{
    public class Camara
    {
        #region Campos y Propiedades
        public const float CAMARA_ZOOM = 1.5f, CENTRO_X = 0.3f, CENTRO_Y = 0.2f;
        public Vector2 Position { get; private set; }
        public float Zoom { get; private set; }
        public float Rotation { get; private set; }
        // Height and width of the viewport window which should be adjusted when the player resizes the game window.
        public int ViewportWidth { get; set; }
        public int ViewportHeight { get; set; }

        // Center of the Viewport does not account for scale
        public Vector2 ViewportCenter
        {
            get
            {
                return new Vector2(ViewportWidth * CENTRO_X, ViewportHeight * CENTRO_Y);
            }
        }
        
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public Camara()
        {
            Zoom = CAMARA_ZOOM;
        }
        
        #endregion

        #region Métodos
        /// <summary>
        /// Centrar la cámara en una coordenada x e y dada
        /// </summary>
        public void CentrarCamara(Vector2 posicionPlayer)
        {
            Position = posicionPlayer;
        }


        /// <summary>
        /// crear una matriz para la cámara para compensar todo lo que dibuje, el mapa y nuestros objetos ya que la 
        /// coordenadas de la cámara son donde la cámara está
        /// </summary>
        public Matrix TranslationMatrix
        {
            get
            {
                return Matrix.CreateTranslation(-(int)Position.X, -(int)Position.Y, 0) *
                   Matrix.CreateRotationZ(Rotation) *
                   Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
                   Matrix.CreateTranslation(new Vector3(ViewportCenter, 0));
            }
        } 
        #endregion
    }
}
