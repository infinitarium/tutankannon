﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Test1;
using GeneracionProcedural;
using System.Collections.Generic;
using Test1.Balas;
using Test1.Content;
using Entidades;
using GeneracionProcedural.Tipos;

namespace Test1
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch _spriteBatch;
        Player _player;
        Room _room;
        Roomie _actual;
        List<Bullet> balas = new List<Bullet>();
        List<Animacion> _explosiones = new List<Animacion>();
        Camara _camara = new Camara();
        Dungeon _dungeon;
        private int POSICION_MAPA_X, POSICION_MAPA_Y;
        int _posXRoom, _posYRoom;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            _dungeon = new Dungeon();
            _room = new Room();
            base.Initialize();
            _camara.ViewportWidth = graphics.GraphicsDevice.Viewport.Width;
            _camara.ViewportHeight = graphics.GraphicsDevice.Viewport.Height;
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            POSICION_MAPA_X = GraphicsDevice.Viewport.TitleSafeArea.X;
            POSICION_MAPA_Y = GraphicsDevice.Viewport.TitleSafeArea.Y + GraphicsDevice.Viewport.TitleSafeArea.Height / 5;
            //cargar las texturas a memoria
            CargarTexturas();
            
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here            
            _room.Initialize(POSICION_MAPA_X, POSICION_MAPA_Y, _dungeon.HabitacionOrigen());
            //guardar las posiciones
            _posXRoom = _dungeon.HabitacionOrigen().PosX;
            _posYRoom = _dungeon.HabitacionOrigen().PosY;
            _actual = _dungeon.Floor[_posXRoom, _posYRoom]; //selecciono el nodo incial
            //Inicializar el player con  su posicion inicial y textura
            _player = new Player(Content, _room.PosPlayer);
            
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            //actualizar el room dependiendo si cambia o no de hab
            
            _room.Update(_player, Keyboard.GetState());

            Direccion inputDireccion = CambiarRoom();
            
            if (inputDireccion != Direccion.Estacionado)
            {
                switch (_dungeon.PuedePasar(new global::Entidades.Player(), _actual, inputDireccion))
                    {
                        case PlayerPasa.Si:
                           GeneracionProcedural.Vector2 desplazamiento = UtilidadesDireccion.ObtenerDesplazamientoDireccion(inputDireccion);
                           _posXRoom += desplazamiento.X;
                           _posYRoom += desplazamiento.Y;
                           _actual = _dungeon.Floor[_posXRoom, _posYRoom];
                           _room.Initialize(POSICION_MAPA_X, POSICION_MAPA_Y, _actual);
                         break;
                        case PlayerPasa.No: //no se puede pasar
                             
                         break;
                        case PlayerPasa.Llave:
                            
                         break;
                    }
            }

            _player.DispararBala(ref balas);
            //ejecutar los update para las explosiones
            for (int i = 0; i < _explosiones.Count; i++)
            {
                _explosiones[i].Update(gameTime);
                //si la explision se acabo, se elimina
                if (_explosiones[i].EndAnimation)
                    _explosiones.Remove(_explosiones[i]);
            }
            
            //ejecurar el update por cada bala del tablero
            for (int i = 0; i < balas.Count; i++)
            {
                balas[i].Update();
                if (_room.BalaEstaColisionando(balas[i], ref _explosiones))
                    balas.Remove(balas[i]);
            }
            //Centrar la cámara en el player
            
            _camara.CentrarCamara(_player.Position);


            base.Update(gameTime);
        }

        private Direccion CambiarRoom()
        {
            KeyboardState teclaPresionada = Keyboard.GetState();
            if (teclaPresionada.IsKeyDown(Keys.A))
            {//A = izquierda
                return Direccion.Arriba;
            }
            if (teclaPresionada.IsKeyDown(Keys.W))
            {//Arriba
                return Direccion.Izquierda;
            }
            if (teclaPresionada.IsKeyDown(Keys.D))
            {//Derecha
                return Direccion.Abajo;
            }
            if (teclaPresionada.IsKeyDown(Keys.S))
            {//Abajo
                return Direccion.Derecha;
            }
            return Direccion.Estacionado;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            // TODO: Add your drawing code here
            _spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, _camara.TranslationMatrix);
            //si el room no ha sido dibujado, lo dibuja
            _room.Draw(_spriteBatch);
            _player.Draw(_spriteBatch);
            //cargar los dibujos de cada bala
            foreach (Bullet bala in balas)
                bala.Draw(_spriteBatch);
            //animar las explosiones
            foreach (Animacion exp in _explosiones)
                exp.Draw(_spriteBatch);
            base.Draw(gameTime);
            _spriteBatch.End();
        }

        private void CargarTexturas()
        {
            //proceso de migración de método
            //cargar las texturas de los bloques
            AssetManager.CargarTile(TipoBloque.Destructible, Content, "Graphics\\SBloque");
            AssetManager.CargarTile(TipoBloque.Indestructible, Content, "Graphics\\SBloqueB");
            AssetManager.CargarTile(TipoBloque.Destructible2, Content, "Graphics\\SBloque2");
            //Pisos
            AssetManager.CargarTile(TipoBloque.Void, Content, "Graphics\\Piso\\Void");
            AssetManager.CargarTile(TipoBloque.Void, Content, "Graphics\\Piso\\Void1");
            AssetManager.CargarTile(TipoBloque.Void, Content, "Graphics\\Piso\\Void2");
            AssetManager.CargarTile(TipoBloque.Void, Content, "Graphics\\Piso\\Void3");
            //cargar la bala
            AssetManager.CargarTextura(Texturas.Bala, Content, "Graphics\\bullet");
            //cargar explosiones
            AssetManager.CargarExplosion(TExplosiones.Bloque, Content);
            //Antorcha (puerta cerrada)
            AssetManager.CargarTile(TipoBloque.LockedDoor, Content, "Graphics\\Antorcha\\a1");
            AssetManager.CargarTile(TipoBloque.LockedDoor, Content, "Graphics\\Antorcha\\a2");
            AssetManager.CargarTile(TipoBloque.LockedDoor, Content, "Graphics\\Antorcha\\a3");
            AssetManager.CargarTile(TipoBloque.LockedDoor, Content, "Graphics\\Antorcha\\a4");
            //puerta abierta (temporal mientras no hay una )
            AssetManager.CargarTile(TipoBloque.Door, Content, "Graphics\\Piso\\Void3");
            //Otros sin gráfica
            AssetManager.CargarTile(TipoBloque.BossDoor, Content,"Graphics\\Pendientes\\Boss");
            AssetManager.CargarTile(TipoBloque.Teasure, Content, "Graphics\\Pendientes\\Teasure");
            AssetManager.CargarTile(TipoBloque.Boss, Content,    "Graphics\\Pendientes\\BossDoor");
            AssetManager.CargarTile(TipoBloque.Key, Content,     "Graphics\\Pendientes\\Key");

        }
    }
}
