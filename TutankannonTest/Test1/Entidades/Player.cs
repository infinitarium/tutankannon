﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using Test1.Entidades;
using GeneracionProcedural;
using Microsoft.Xna.Framework.Content;
using Test1.Balas;
using System.Collections.Generic;
using Test1.Content;
using Vector2 = Microsoft.Xna.Framework.Vector2;
using Test1.Utilidades;

namespace Test1
{
    public class Player : Vehiculo
    {
        const int FRAMES_POR_SEGUNDO = 1;
        public Texture2D PlayerTexture { get; set; }

        
        public Player(ContentManager content, int[] posPlayer)
            : base(posPlayer[0], posPlayer[1]) 
        {
            Initialize(content, posPlayer);
        }
        
        /// <summary>
        /// Inicializador para el jugador, recibe sus valores iniciales
        /// </summary>
        /// <param name="texture">Textura a dibujar del player</param>
        /// <param name="initialPosition">Posicion dentro del room donde se dibujará el player</param>
        private void Initialize(ContentManager content, int[] posPlayer)
        {
            Entidad = Entidad.Player;
            Posicion = new Vector2(posPlayer[0], posPlayer[1]);
            Health = 100;
            //posiciones dentro del room
            PosX = posPlayer[2];
            PosY = posPlayer[3];
            Aceleracion = 0.5f;
            MaxVelocidad = 2f;
            //Se deja al player dentro de su posición del cuarto actual
            CargarTextura(content);
            PlayAnimation("Derecha");
        }

        /// <summary>
        /// Carga la textura del player
        /// </summary>
        /// <param name="content"></param>
        private void CargarTextura(ContentManager content)
        {
            FramesPerSecond = FRAMES_POR_SEGUNDO;
            sTexture = content.Load<Texture2D>("Graphics\\smalltank");
            AddAnimation(2, 0, 0, "Abajo", 16, 16, new Vector2(0, 0));
            AddAnimation(2, 0, 3, "Derecha", 16, 16, new Vector2(0, 0));
            AddAnimation(2, 16, 0, "Arriba", 16, 16, new Vector2(0, 0));
            AddAnimation(2, 16, 3, "Izquierda", 16, 16, new Vector2(0, 0));
        }

        /// <summary>
        /// Se llama     por cada frame
        /// </summary>
        /// <param name="teclaPresionada">tecla recibida para el movimiento del personaje</param>
        public void Update(ref Entidad[,] mapaEntidades, ref TipoBloque[,] bloques, int TILE_SIZE) 
        {
            List<Bullet> balas = new List<Bullet>();
            //llama a su versión sin parámetros
            Update();
            //Está la opción de dejar presionado shift para cambiar dirección sin moverse
            if (InputManager.DetectarInput(Entradas.Estacionar) && Moviendose == false)
            {
                //LLama a la función mover player dependiendo además de la tecla presionada
                if (InputManager.DetectarInput(Entradas.Arriba))
                    Direccion = Direccion.Arriba;
                else if (InputManager.DetectarInput(Entradas.Abajo))
                    Direccion = Direccion.Abajo;
                else if (InputManager.DetectarInput(Entradas.Izquierda))
                    Direccion = Direccion.Izquierda;
                else if (InputManager.DetectarInput(Entradas.Derecha))
                    Direccion = Direccion.Derecha;
            }
            //LLama a la función mover player dependiendo además de la tecla presionada
            else if (InputManager.DetectarInput(Entradas.Arriba))
                MoverVehiculo(Direccion.Arriba, ref mapaEntidades, ref bloques, TILE_SIZE);
            else if (InputManager.DetectarInput(Entradas.Abajo))
                MoverVehiculo(Direccion.Abajo, ref mapaEntidades, ref bloques, TILE_SIZE);
            else if (InputManager.DetectarInput(Entradas.Izquierda))
                MoverVehiculo(Direccion.Izquierda, ref mapaEntidades, ref bloques, TILE_SIZE);
            else if (InputManager.DetectarInput(Entradas.Derecha))
                MoverVehiculo(Direccion.Derecha, ref mapaEntidades, ref bloques, TILE_SIZE);
            //cargar animacion dependiendo de la direccion (siempre que no sea entacionado)
            if (Direccion != Direccion.Estacionado)
            {
                PlayAnimation(Direccion.ToString());
            }
            //PlayAnimation(Direccion != Direccion.Estacionado ? Direccion.ToString() : Direccion.Derecha.ToString());
            //mueve la entidad usando el algoritmo de movimiento tileado
            EjercutarMovimientoTileado();            
        }

        /// <summary>
        /// Dispara y actualiza el movimiento de la bala
        /// </summary>
        /// <param name="balas"></param>
        /// <param name="teclaPresionada"></param>
        public void DispararBala(ref List<Bullet> balas) 
        {
            Bullet bala = new Bullet(); ;
            //Ejecutar disparo
            if (InputManager.DetectarInput(Entradas.Disparar))
            {
                //solo dispara si corresponde con el cooldonw
                if (Cooldown <= 0)
                {
                    //restaura el cooldown
                    Cooldown = CadenciaDisparo;
                    //crea la bala que es disparada
                    bala = new Bullet
                        (
                            Posicion, 8, Team.Protectores, Direccion
                        );
                    balas.Add(bala);
                }
            }
        }
       
        /// <summary>
        /// Runs every time an animation has finished playing
        /// </summary>
        /// <param name="AnimationName">Name of the ended animation</param>
        public override void AnimationDone(string animation)
        {
            
        }   
    }
}
