﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Test1.Content
{
    public class Animacion : IDrawable
    {
        #region Campos y propiedades
        private List<Texture2D> _frames;
        private double tiempoTranscurrido;
        private double timeToUpdate;
        public Vector2 Position { get; set; }
        public bool Paused { get; set; }
        public bool Loop { get; set; }
        private bool _endAnimation;
        private int _frameIndex;

        /// <summary>
        /// indica el indice del frame actual
        /// </summary>
        public int FrameIndex
        {
            get { return _frameIndex; }
            set 
            {
                //Si el frame index se cambia a un valor que no es el 
                //final de la animación, entonces cambia el endAnimation a false
                _endAnimation = value == _frames.Count - 1 ? true : false;
                _frameIndex = value; 
            }
        }

        /// <summary>
        /// Indica si la animación ya a terminado (cargó su último frame)
        /// </summary>
        public bool EndAnimation 
        {
            get 
            {
                return _endAnimation;
            }
        }

        /// <summary>
        /// Velocidad de la animacion
        /// </summary>
        public double FramesPorSegundo
        {
            set { timeToUpdate = (1f / value); }
        }
        #endregion

        #region Constructor e Inicializador
        public Animacion()
        {
            Inicializador();
        }

        public Animacion(Vector2 position)
            : this()
        {
            Position = position;
            //cantidad por defecto de los frames por segundo
            FramesPorSegundo = 12;
        }

        private void Inicializador()
        {
            _frames = new List<Texture2D>();
            Paused = false;
            Loop = false;
        } 
        #endregion

        /// <summary>
        /// Añade un Frame a la animación
        /// </summary>
        /// <param name="textura">textura de frame que se va a agregar</param>
        /// <returns>true si se puede, false si se falla al ingresar al animación</returns>
        public bool AddFrame(Texture2D textura) 
        {
            try
            { //añade un frame
                _frames.Add(textura);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Actualiza el estado de la animación
        /// </summary>
        public void Update(GameTime gameTime) 
        {
            //añade el tiempo trascurrido desde el último frame
            tiempoTranscurrido += gameTime.ElapsedGameTime.TotalSeconds;

            //Si el tiempo transcurrido es mayor al tiempo para actualizar, cambia el frame
            if (tiempoTranscurrido > timeToUpdate)
            {
                //El tiempo transcurrido vuelve al tiempo de actualización para mantener 
                //el numero de frames por segundo deseado
                tiempoTranscurrido -= timeToUpdate;

                //aumenta en 1 el frame index
                if (_frameIndex < _frames.Count - 1)
                    _frameIndex++;
                //si tiene activado bucle, reininica la animación, sino no
                else if (Loop)
                    _frameIndex = 0;
                else
                    _endAnimation = true;
            }
        }

        /// <summary>
        /// Dibuja en pantalla el frame actual
        /// </summary>
        /// <param name="spriteBatch"></param>
        public void Draw(SpriteBatch spriteBatch) 
        {
            spriteBatch.Draw
                (
                _frames[_frameIndex],
                Position, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f
                );
        }

        
    }
}
