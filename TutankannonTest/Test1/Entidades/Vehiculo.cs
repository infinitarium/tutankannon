﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneracionProcedural;
using Microsoft.Xna.Framework;
using Vector2 = Microsoft.Xna.Framework.Vector2;

namespace Test1.Entidades
{
    public abstract class Vehiculo : SpriteAnimado
    {
        #region Campos y Propiedades
        protected bool Moviendose { get; set; }
        protected float DistanciaPorMover { get; set; }
        public Direccion Direccion { get; set; }
        public float Velocidad { get; set; }
        public float MaxVelocidad { get; set; }
        public int Cooldown { get; set; }
        public int CadenciaDisparo { get; set; }
        public Team Team { get; set; }
        public float Aceleracion { get; set; }
        public int Health { get; set; }
        public bool Active { get; set; }
        public int PosX { get; set; }
        public int PosY { get; set; }
        public Entidad Entidad { get; set; }
        
        #endregion

        #region Constructor e Inicializador
        public Vehiculo(int xInicial, int yInicial) : base(new Vector2(xInicial, yInicial))
        {
            Inicializador();
        }

        private void Inicializador()
        {
            Velocidad = 0;
            MaxVelocidad = 5;
            Aceleracion = 0.5f;
            DistanciaPorMover = 0;
            Moviendose = false;
            Active = true;
            Direccion = Direccion.Estacionado;
            CadenciaDisparo = 20;
        }
        
        #endregion

        #region Metodos "Protegidos"
        /// <summary>
        /// Permite el movimiento por tiles (pasar a clase vehiculo)
        /// </summary>
        protected void EjercutarMovimientoTileado()
        {
            if (Moviendose && DistanciaPorMover > 0)
            {
                //aumenta la velocidad en base a la aceleración
                if (Velocidad < MaxVelocidad)
                {
                    Velocidad += Aceleracion;
                }
                //dependiendo de la dirección, cambiará la coordenada
                switch (Direccion)
                {
                    case Direccion.Arriba:
                        Posicion.Y -= Velocidad;
                        break;
                    case Direccion.Abajo:
                        Posicion.Y += Velocidad;
                        break;
                    case Direccion.Izquierda:
                        Posicion.X -= Velocidad;
                        break;
                    case Direccion.Derecha:
                        Posicion.X += Velocidad;
                        break;
                }
                //lo avanzado se resta a la distancia recorrida
                DistanciaPorMover -= Velocidad;
            }
            else if (DistanciaPorMover <= 0)
            {
                //detiene el movimiento
                Moviendose = false;
                //Ajuste por los pixeles que se ha pasado
                switch (Direccion)
                {
                    case Direccion.Arriba:
                        Posicion.Y -= DistanciaPorMover;
                        break;
                    case Direccion.Abajo:
                        Posicion.Y += DistanciaPorMover;
                        break;
                    case Direccion.Izquierda:
                        Posicion.X -= DistanciaPorMover;
                        break;
                    case Direccion.Derecha:
                        Posicion.X += DistanciaPorMover;
                        break;
                }
                DistanciaPorMover = 0;
            }
            else if (Moviendose == false)
            {
                if (Velocidad > 0)
                {
                    Velocidad -= Aceleracion;
                }
            }
        }

        /// <summary>
        /// Cambiar la dirección del vehículo y aumentar 
        /// su vaor de distancia a recorrer
        /// </summary>
        /// <param name="direccion">Dirección donde se quiere mover</param>
        protected void MoverVehiculo(Direccion direccion, ref Entidad[,] mapaEntidades, ref TipoBloque[,] bloques, int TILE_SIZE)
        {
            //Solo comienza a moverse cuando
            //1) no se está moviendo y
            //2)No va a chocar con un bloque o enemigo
            if (!Moviendose)
            {
                //sino se está moviendo, puede cambiar la dirección.
                Direccion = direccion;
                //solo dispone a moverse si es posible moverse
                if (EsPosibleMoverse(ref mapaEntidades, ref bloques))
                {
                    int nuevoX = PosX, nuevoY = PosY;
                    //ahora cambia a moverse
                    Moviendose = true;
                    DistanciaPorMover = TILE_SIZE;
                    //cambiar la coordenada en el mapa de coordenada
                    mapaEntidades[PosX, PosY] = Entidad.Void;
                    //mover de acuerdo a la dirección
                    Trasladar(Direccion, ref nuevoX, ref nuevoY);
                    PosX = nuevoX;
                    PosY = nuevoY;
                    //colocar la entidad que corresponde
                    mapaEntidades[PosX, PosY] = Entidad;
                }
            }
        }

        /// <summary>
        /// Cambia la dirección x o y de acuerdo a la dirección dada
        /// </summary>
        /// <param name="direccion">Dirección dada para el movimiento</param>
        /// <param name="x">coordenada x inicial a cambiar</param>
        /// <param name="y">coordenada y inicial a cambiar</param>
        protected void Trasladar(Direccion direccion, ref int x, ref int y)
        {
            switch (direccion)
            {
                case Direccion.Arriba:
                    y--;
                    break;
                case Direccion.Derecha:
                    x++;
                    break;
                case Direccion.Abajo:
                    y++;
                    break;
                case Direccion.Izquierda:
                    x--;
                    break;
            }
        }

        /// <summary>
        /// Acciones que se ejecutan cada por cada frame del vehículo
        /// </summary>
        public void Update() 
        {
            //por cada actualización reduce en un el cooldown pra quepueda disparar
            if (Cooldown > 0) Cooldown--;
        }


        /// <summary>
        /// Verificar si es posible avanzar en una dirección dada
        /// </summary>
        /// <returns>True = es posible avanzar, False, no es posible avanzar</returns>
        private bool EsPosibleMoverse(ref Entidad[,] mapaEntidades, ref TipoBloque[,] bloques)
        {
            int posx = PosX, posy = PosY;
            //verifica la posición futura
            Trasladar(Direccion, ref posx, ref posy);
            return 
                //no se sale de la pantalla
                posx >= 0 && posy >= 0 && posx < bloques.GetLength(0) && posy < bloques.GetLength(1) &&
                //no hay nadie enemigo bloqueando 
                (mapaEntidades[posx, posy] == Entidad.Void || mapaEntidades[posx, posy] == Entidad.Ally) && 
                //no hay un bloque bloqueando
                (bloques[posx, posy] == TipoBloque.Void || bloques[posx, posy] == TipoBloque.Traspasabe || bloques[posx, posy] == TipoBloque.Door)
                ? true : false;
        }   
        #endregion

        /// <summary>
        /// Posicionar el sprite en una posicion
        /// </summary>
        /// <param name="posicion"></param>
        public void Posicionar(Vector2 posicion) 
        {
            Posicion.X = posicion.X;
            Posicion.Y = posicion.Y;
        }
    }
}
