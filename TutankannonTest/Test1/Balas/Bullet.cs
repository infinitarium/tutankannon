﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneracionProcedural;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Vector2 = Microsoft.Xna.Framework.Vector2;

namespace Test1.Balas
{
    public class Bullet : IDrawable
    {
        #region Campos y Propiedades
        public int Velocidad { get; set; }
        public Team Team { get; set; }
        public Direccion Direccion { get; set; }
        public Vector2 Posicion; 
        #endregion

        #region Cosntructores
        /// <summary>
        /// Las balas deben crearse con sus parámetros correctamente
        /// </summary>
        public Bullet()
        {
            Direccion = Direccion.Estacionado;
        }

        public Bullet(Vector2 posicion, int velocidad, Team team, Direccion direccion)
        {
            Posicion = posicion;
            Velocidad = velocidad;
            Team = team;
            Direccion = direccion;
        } 
        #endregion

        #region Metodos Públicos
        /// <summary>
        /// Indica si la bala esta colisionando, además de la posición x e y del 
        /// room de donde se encuentra
        /// </summary>
        /// <param name="room">room de donde se lanzó la bala</param>
        /// <param name="posX">Coordenada X del rooom donde se realizó el impacto</param>
        /// <param name="posY">Coordenada Y del rooom donde se realizó el impacto</param>
        /// <returns>Verdadero: Bala impactó, Falso: Bala no impactó</returns>
        public bool BalaEstaColisionando(Room room, out int posXBala, out int posYBala, out TipoBloque bloqueBala) 
        {
            //Calcula las posiciones X e Y de la bala
            posXBala = (int)((Posicion.X - room.EsquinaInferior[0]) / room.TILE_SIZE);
            posYBala = (int)((Posicion.Y - room.EsquinaInferior[1]) / room.TILE_SIZE);
            bloqueBala = room._bloquesRoom[posXBala, posYBala];
            //Si el bloque está vacío retorna false
            //TO DO: Ajustarlo para las entidades también (jugador, enemigo, etc)
            if (bloqueBala == TipoBloque.Void)
                return false;
            return true;
        }
        
        
        /// <summary>
        /// Dibuja la bala en pantalla
        /// </summary>
        public void Draw(SpriteBatch spriteBatch)
        {
            if (Direccion == Direccion.Estacionado) return;

            spriteBatch.Draw
                (
                //depende del tipo de bloque                            
                    AssetManager.GetTextura(Texturas.Bala),
                    Posicion, null, Color.White, 0f, Vector2.Zero, 1f, SpriteEffects.None, 0f
                );
        }

        /// <summary>
        /// La bala se mueve dependiendo de su velocidad y 
        /// dirección
        /// </summary>
        public void Update()
        {
            //dependiendo de la dirección, cambiará la coordenada
            switch (Direccion)
            {
                case Direccion.Arriba:
                    Posicion.Y -= Velocidad;
                    break;
                case Direccion.Abajo:
                    Posicion.Y += Velocidad;
                    break;
                case Direccion.Izquierda:
                    Posicion.X -= Velocidad;
                    break;
                case Direccion.Derecha:
                    Posicion.X += Velocidad;
                    break;
                case Direccion.Estacionado:
                    break;
            }
        } 
        #endregion

    }
}
