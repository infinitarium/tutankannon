﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneracionProcedural;
using Entidades;
using GeneracionProcedural.Tipos;

namespace ConsolaDePruebas
{
    class Program
    {
        static void Main(string[] args)
        {
            Random _aleatorio = new Random();
            Dungeon dungeon = new Dungeon();
            Roomie room = new Roomie();
            int generados = 0;            
            //mostrar el _escenario en pantalla
            for (;;)
            {
                Console.Clear();
                //Navegar();               
                Navegar();
                Console.WriteLine("Generados {0}", ++generados);
                Console.ReadKey();
            }
        }

        private static void Navegar()
        {
            Player player = new Player();
            Dungeon dungeon = new Dungeon();
            int posX = dungeon.TDungeon.Root.Value.PosX, posY = dungeon.TDungeon.Root.Value.PosY;
            Roomie actual = dungeon.Floor[posX, posY]; //selecciono el nodo incial
            ConsoleKeyInfo cki; //para guardar la información de consola        
            //desplazamientos en las direccion posibles
            Vector2[] DesplazamientoDireccion = new Vector2[4]
            {
                new Vector2(-1, 0), // Arriba
                new Vector2(0, +1), // Derecha
                new Vector2(+1,0), // Abajo
                new Vector2(0, -1)  // Izquierda
            };

            for (; ; )
            {
                Console.Clear();
                //indicar la cantidad de llaves
                Console.WriteLine("Llaves: {0}", player.Keys);
                //dibujar la habitacion
                DrawRoomie(actual);
                //mostrar mensaje si tomó una llave
                if (actual.Seccion == Secciones.TK)
                {
                    MensajeRojo("!Has tomado una llave!");
                    player.Keys++;
                    actual.Seccion = Secciones.K; //ya no es un cuarto especial
                }
                //LLegó al jefe
                if (actual.Seccion == Secciones.TB)
                {
                    MensajeRojo("!Felicidades! Has derrotado al jefe");
                    Console.ReadKey();
                    break;
                }
                //dibujar el _escenario
                DrawDungeon(dungeon, posX, posY);
                //mostrar que puertas estan habilitadas
                foreach (EstadoPuerta item in actual.Doors)
                    Console.WriteLine("{0} ", item);
                //para moverse en el _escenario
                cki = Console.ReadKey();

                Direccion inputDireccion = GetDireccion(cki);
                //verificar que se presionó una dirección válida
                if (inputDireccion != Direccion.Estacionado)
                {   //si la puerta se pasable (true)
                    switch (dungeon.PuedePasar(player, actual, inputDireccion))
                    {
                        case PlayerPasa.Si:
                            Console.Clear();
                            Vector2 desplazamiento = DesplazamientoDireccion[(int)inputDireccion];
                            posX += desplazamiento.X;
                            posY += desplazamiento.Y;
                            actual = dungeon.Floor[posX, posY];
                         break;
                        case PlayerPasa.No: //no se puede pasar
                             MensajeRojo("NO se puede avanzar en esa dirección, presione tecla para continuar");
                             Console.ReadKey();
                         break;
                        case PlayerPasa.Llave:
                            //se gasta la llave y se abre la puerta :D
                            player.Keys--;
                            actual.CrearPuerta(inputDireccion, true, TipoBloque.Door);
                            Console.Clear();
                            MensajeRojo("Se usó la llave para entrar");
                            Console.ReadKey();
                            Vector2 desplazamiento1 = DesplazamientoDireccion[(int)inputDireccion];
                            posX += desplazamiento1.X;
                            posY += desplazamiento1.Y;
                            actual = dungeon.Floor[posX, posY];
                         break;
                    }
                }
                else
                {//el usuario coloco la letra escape para repetir
                    if (cki.Key == ConsoleKey.Escape) break;
                }
            } //end for
        }

        /// <summary>
        /// Muestra un mensaje de color rojo y luego regresa el color 
        /// de consola a su color habitual
        /// </summary>
        /// <param name="mensaje">Mensaje a mostrar</param>
        private static void MensajeRojo(string mensaje)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(mensaje);
            Console.ResetColor();
        }

        /// <summary>
        /// Retorna la direccion (enum) correspondiente a una flecha direccionadora
        /// que el usuario haya ingresao
        /// </summary>
        /// <param name="cki">Input de tecla que haya ingresado el usuario</param>
        /// <returns>Enum de Direcciones indicando la dirección indicada x el usuario</returns>
        private static Direccion GetDireccion(ConsoleKeyInfo cki)
        {
            if (cki.Key == ConsoleKey.UpArrow)
                return Direccion.Arriba;
            else if (cki.Key == ConsoleKey.DownArrow )
                return Direccion.Abajo;
            else if (cki.Key == ConsoleKey.LeftArrow )
                return Direccion.Izquierda;
            else if (cki.Key == ConsoleKey.RightArrow )
                return Direccion.Derecha;
            else
                return Direccion.Estacionado;
        }

        private static void DrawRoomie(Roomie room)
        {
            for (int i = 0; i < room.Room.GetLength(0); i++)
            {
                for (int j = 0; j < room.Room.GetLength(1); j++)
                {
                    Console.BackgroundColor = (ConsoleColor)room.Room[i, j];
                    Console.Write(" ");
                }
                Console.WriteLine();
            }
            Console.ResetColor();
        }
       
        /// <summary>
        /// Función que dibuja un arreglo de Dungeon en pantalla
        /// La idea es hacer esto mismo pero con cubos 3D
        /// </summary>
        /// <param name="_escenario">calabozo a dibujar</param>
        /// <param name="posX">coordX del jugador</param>
        /// <param name="posY">coordY del jugador</param>
        private static void DrawDungeon(Dungeon dungeon, int posX, int posY)
        {
            string dibujo = string.Empty;
            Console.WriteLine("S = S, K = Key, Lock = L, B = B\n\n");
            for (int i = 0; i < dungeon.Floor.GetLength(0); i++)
            {
                for (int j = 0; j < dungeon.Floor.GetLength(1); j++)
                {
                    Console.BackgroundColor = ((int)dungeon.Floor[i, j].Seccion) == (int)Secciones.N ? ConsoleColor.Black : (ConsoleColor)dungeon.Floor[i, j].Seccion;
                    
                    //Selecciona la grafica a mostrar de acuerdo al tipo de habitacion y/o seccion
                    switch ((int)dungeon.Floor[i, j].Seccion)
                    {
                        case 0:
                        case -1:
                            dibujo = " ";
                            break;
                        case (int)Secciones.TB:
                            dibujo = "@";
                            break;
                        case (int)Secciones.TK:
                            dibujo = "#";
                            break;
                        case (int)Secciones.TT:
                            dibujo = "O";
                            break;
                        default:
                            dibujo = (dungeon.Floor[i, j].Seccion).ToString();
                            break;
                    }
                    //revisa la posicion del jugador y la marca con color y simbolo
                    if (posX == i && posY == j)
                    {
                        Console.BackgroundColor = ConsoleColor.Blue;
                        dibujo = "P";
                    }
                    Console.Write("{0} ", dibujo);
                }
                Console.WriteLine();
            }
            Console.ResetColor();
        }


        /// <summary>
        /// Función que dibuja un arreglo de Dungeon en pantalla
        /// La idea es hacer esto mismo pero con cubos 3D
        /// </summary>
        /// <param name="_escenario">piso a dibujar</param>
        private static void DrawDungeon(Dungeon dungeon)
        {
            string dibujo = string.Empty;
            Console.WriteLine("S = S, K = Key, Lock = L, B = B\n\n");
            for (int i = 0; i < dungeon.Floor.GetLength(0); i++)
            {
                for (int j = 0; j < dungeon.Floor.GetLength(1); j++)
                {
                    Console.BackgroundColor = ((int)dungeon.Floor[i, j].Seccion) == (int)Secciones.N ? ConsoleColor.Black : (ConsoleColor)dungeon.Floor[i, j].Seccion;
                    //Selecciona la grafica a mostrar de acuerdo al tipo de habitacion y/o seccion
                    switch ((int)dungeon.Floor[i, j].Seccion)
                    {
                        case 0:
                        case -1:
                            dibujo = " ";
                            break;
                        case (int)Secciones.TB:
                            dibujo = "@";
                            break;
                        case (int)Secciones.TK:
                            dibujo = "#";
                            break;
                        default:
                            dibujo = (dungeon.Floor[i, j].Seccion).ToString();
                            break;
                    }
                    Console.Write("{0} ", dibujo);
                }
                Console.WriteLine();
            }
            Console.ResetColor();
        }
    }
}
