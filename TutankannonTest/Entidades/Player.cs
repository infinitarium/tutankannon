﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Entidades
{
    public class Player
    {
        public int Keys { get; set; } //Cantidad de llaves del jugador

        #region Constructor e Inicializador
        public Player()
        {
            Initialize();
        }

        private void Initialize()
        {
            Keys = 0;
        } 
        #endregion
    }
}
