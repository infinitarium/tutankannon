﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneracionProcedural.Tipos;
using System.Diagnostics;
using Entidades;

namespace GeneracionProcedural
{
    /// <summary>
    /// Clase generadora de Piso completo del _escenario. TO DO: Implementar niveles de piso
    /// </summary>
    public class Dungeon
    {
        #region Campos y Propiedades
        const int IZQUIERDA = 0, DERECHA = 1, AMBOS = 2, PADRE = 3;
        private BinaryTree<Roomie> _dungeon; //árbol binario del _escenario. Cada conexión con un nodo es sinónimo a una puerta (máx 3 hab2 por nodo: Padre y dos hijos)
        private Random _aleatorio = new Random();
        private Roomie[,] _escenario = new Roomie[20, 20]; //Arreglo donde se dibujará el o los pisos. Tiene un tamaño máximo y el generador no puede construir fuera de él
        public int cantidadLocks { get; set; }
        public int cantidadKeys { get; set; }
        public int cantidadBoss { get; set; }
        public int profLocks { get; set; }
        public int profKeys { get; set; }
        public int profBoss { get; set; }


        /// <summary>
        /// El valor del piso o escenario solo se puede modificar internamente en la clase
        /// pero si se puede obtener sus datos para dibujar
        /// </summary>
        public Roomie[,] Floor
        {
            get { return _escenario; }
        }
        /// <summary>
        /// Se puede acceder al árbol del _escenario, pero es 
        /// de solo lectura (solo se puede modificar llamando 
        /// al método generar _escenario (TDungeon = Tree Dungeon
        /// </summary>
        public BinaryTree<Roomie> TDungeon 
        {
            get { return _dungeon; }
        }
        #endregion

        #region Constructor e Inicizalidor
        public Dungeon()
        {
            Init();
            GenerarDungeon();
        }

        /// <summary>
        /// Inicializador de variables
        /// </summary>
        private void Init()
        {
            _dungeon = new BinaryTree<Roomie>();
           // _aleatorio = new Random();
            _escenario = new Roomie[20, 20];
            InicializarEscenario();
            cantidadBoss = 1;
            cantidadKeys = 2;
            cantidadLocks = 1;
            profLocks = 4;
            profKeys = 4;
            profBoss = 4;
        }

        /// <summary>
        /// Inicializa el escenerio respetando las siguientes reglas:
        /// 1) los espacios vacios se rellenan con ceros 
        /// 2) Hay cierta probabilidad (testing 75%) que aparezca un bloque invisible 
        /// donde no se puede generar cuarto en un espacio par de fila y columna. Esto es 
        /// para aumentar la probabilidad de crear pasillos
        /// </summary>
        private void InicializarEscenario()
        {
            for (int i = 0; i < _escenario.GetLength(0); i++)
                for (int j = 0; j < _escenario.GetLength(1); j++)
                {
                    //se colocan espacios vacios y bloqueados para ayudar a la construcción del _dungeon
                    _escenario[i, j] = new Roomie()
                    {
                        Seccion = (Secciones)(_aleatorio.Next(0, 4) <= 2 && i % 2 == 0 && j % 2 == 0 ? (int)Secciones.N : 0)
                    };
                }
        }
        #endregion

        #region Metodos Publicos
        /// <summary>
        /// Crea el _escenario usando el árbol binario
        /// </summary>
        public void GenerarDungeon()
        {
            try
            {
                int posX = 0, posY = 0, cantSecciones = cantidadBoss + cantidadLocks;
                InicializarEscenario();
                //se crea este pasillo inicial para evitar que ambas secciones se bloqueen entre si
                Roomie inicio = new Roomie() { PosX = _escenario.GetLength(0) / 2, PosY = _escenario.GetLength(1) / 2, Seccion = Secciones.S };
                //Genera las primeras habitaciones para el _escenario
                _dungeon.Root = new BinaryTreeNode<Roomie>(inicio);
                BinaryTreeNode<Roomie> start = _dungeon.Root;
                _escenario[inicio.PosX, inicio.PosY] = inicio;

                //Elije una coordenada inicial para el primer pasillo
                GetDisponible(start, out posX, out posY);
                CrearHabitacion(ref start, IZQUIERDA, posX, posY, Secciones.K);

                
                //Elije una coordenada inicial para el segundo pasillo
                GetDisponible(start, out posX, out posY);
                CrearHabitacion(ref start, DERECHA, posX, posY, Secciones.B);
                //Se actualiza el valor del calabozo
                _dungeon.Root = start;

                //Función recursiva que crea pasillos hasta completar la profundidad dada por el segundo parámetro
                GenerarPasilloLineal(_dungeon.Root.Left, Secciones.K, profKeys);
                GenerarPasilloLineal(_dungeon.Root.Right, Secciones.B, profBoss);

                //Funcion que crea pasillo a partir de otros
                BinaryTreeNode<Roomie> cerrado = GenerarPasilloHijo(_dungeon.Root.Left, Secciones.L, profLocks);
                GenerarPasilloHijo(cerrado, Secciones.K, profKeys);
            }
            catch (Exception ex)
            {
                DrawDungeon();
                Console.WriteLine(ex.Message);   
            }
        }

        /// <summary>
        /// Retorna la primera hab del dungeon
        /// </summary>
        /// <returns></returns>
        public Roomie HabitacionOrigen() 
        {
            return _escenario[TDungeon.Root.Value.PosX, TDungeon.Root.Value.PosY];
        }

        
        /// <summary>
        /// Indica si se puede pasar de una habitación a otra
        /// </summary>
        /// <param name="player">jugador, se usará para ver si posee llaves, itemes especiales, etc</param>
        /// <param name="origen">Habitación de origen</param>
        /// <param name="direccion">Direccion a la que se quiere mover</param>
        /// <returns></returns>
        public PlayerPasa PuedePasar(Player player, Roomie origen, Direccion direccion) 
        {
           //puede pasar porque la puerta está abierta 
            if (origen.Doors[(int)direccion] == EstadoPuerta.Abierta)
                return PlayerPasa.Si;
            //puede pasar pero usando una llave
            else if (origen.Doors[(int)direccion] == EstadoPuerta.Cerrada && player.Keys > 0) 
                return PlayerPasa.Llave;
            //no puede pasar ya que o no hay puerta o esta está cerrada
            else
                return PlayerPasa.No;
        }
        #endregion
        
        #region Metodos Privados                      
        /// <summary>
        /// Genera un algoritmo utilizando algoritmos no recursivo. La idea es controlar directamente la 
        /// cantidad de cuartos que se generar en dicho pasillo
        /// </summary>
        /// <param name="room">La rooom de incio para el pasillo</param>
        /// <param name="secciones">La sección del pasillo a crear</param>
        /// <param name="profundidad">Cantidad de rooms que se crearán del pasillo dado</param>
        private void GenerarPasilloLineal(BinaryTreeNode<Roomie> room, Secciones secciones, int profundidad) 
        {
            int coordX, coordY, avance = 0, intentos = 0; //avance 0 = izq y entra, 1 = izq, derec y entra, 2 crea un hijo donde esra
            BinaryTreeNode<Roomie> padre = room; //padre se crea para poder volver a la raíz del árbol
            
            #region Creacion de habitaciones
            //se itera hasta crear la cantidad de habitaciones posibles a crear
            for (int i = 0; i < profundidad; )
            {
                bool creado = false; //en cada iteracion se resetea el bool que indica si se ha creado o no una habitación
                room = padre; //en cada iteracion se vuelve al nodo padre
                while (!creado) //el algorimo persiste hasta que crea una habitacion
                {
                    avance = _aleatorio.Next(0, 3);
                    //si no tiene ningun hijo, crea inmediatamente un nodo
                    if (room.Left == null && room.Right == null && GetDisponible(room, out coordX, out coordY))
                    {
                        CrearHabitacion(ref room, IZQUIERDA, coordX, coordY, secciones, out creado, ref i);
                        break; //sale del ciclo porque ya creo el hijo ;)
                    }
                    //si se decidio ir por la izquieda
                    if (avance == IZQUIERDA)
                    {
                        //si se va por la izquierda y no hay nodo se intenta crear uno
                        if (room.Left == null)
                        {
                            //ve si es posible crear un nodo a la izquieda
                            if (GetDisponible(room, out coordX, out coordY))
                            {
                                CrearHabitacion(ref room, IZQUIERDA, coordX, coordY, secciones, out creado, ref i);
                                break; // se sale del ciclo
                            }
                            else
                                break; //no se puede seguir avanzando por aca
                        }
                        else
                            //si ya habia un nodo a la izquierda, se entra por esa habitacion para seguir generando
                            room = room.Left;
                    }
                    else if (avance == DERECHA)
                    {
                        //Si no hay nodo a la derecha, se intenta crear uno
                        if (room.Right == null)
                        {
                            //su se puede crear un nodo a la derecha, se crea
                            if (GetDisponible(room, out coordX, out coordY))
                            {
                                CrearHabitacion(ref room, IZQUIERDA, coordX, coordY, secciones, out creado, ref i);
                                break; //salir del ciclo
                            }
                            else
                                break;
                        }
                        else
                            //si ya habia un nodo a la derecha, entonces se avanza por el 
                            room = room.Right;
                    }
                    //Se colocan hijos para ambos lados
                    else if (avance == AMBOS)
                    {
                        if (room.Left == null)
                        {
                            //intenta crear un hijo a la izquierda
                            if (GetDisponible(room, out coordX, out coordY))
                                CrearHabitacion(ref room, IZQUIERDA, coordX, coordY, secciones, out creado, ref i);
                            else
                                break;
                        }
                        if (room.Right == null)
                        {
                            //intenta crear además un hijo a la derecha siempre y cuando no nos hayamos pasado en la profundidad
                            if (GetDisponible(room, out coordX, out coordY) && (i + 1) < profundidad)
                            {
                                CrearHabitacion(ref room, IZQUIERDA, coordX, coordY, secciones, out creado, ref i);
                                break;
                            }
                            else
                                break;
                        }
                    }
                } 
                //en caso que sea impsible crear otro room, (pegado) se termina el ciclo
                if (intentos++ > profundidad * 7)
                    break;
            }
            #endregion
            
            //Si son pasillos donde hay que insertar algo dentro (jefe, llave, premio, lo inserta)
            if (secciones == Secciones.K || secciones == Secciones.B || secciones == Secciones.L)
                InsertarEspecialRoom(padre, secciones);
            
        }

        /// <summary>
        /// Crear habitacion para el metodo generar Pasillo Linea
        /// </summary>
        /// <param name="creado">Indica que se ha creado la room</param>
        /// <param name="i">aumenta el contador</param>
        private void CrearHabitacion(ref BinaryTreeNode<Roomie> room, int IZQUIERDA, int coordX, int coordY, Secciones secciones, out bool creado, ref int i)
        {
            CrearHabitacion(ref room, DERECHA, coordX, coordY, secciones);
            creado = true;
            i++;
        }

        /// <summary>
        /// Crea una habitacion en las coordenadas especificadas unida a otra habitación padre
        /// </summary>
        /// <param name="padre">Habitación padre que irá conectada a la nueva habitación a crear</param>
        /// <param name="direccion">Dirección en relación a la habitación padre donde se creará la habitación nueva</param>
        /// <param name="coordX">Coordenada X donde se creará la nueva hab</param>
        /// <param name="coordY">Coordenada Y donde se creará la nueva hab</param>
        /// <param name="secciones">Sección de la habitacion a crear</param>
        private void CrearHabitacion(ref BinaryTreeNode<Roomie> padre, int direccion, int coordX, int coordY, Secciones secciones)
        {
            Roomie hab = new Roomie() { PosX = coordX, PosY = coordY, Seccion = secciones };
            bool estadoDoor = true;
            TipoBloque tipoPuerta = TipoBloque.Door;

            BinaryTreeNode<Roomie> hijo = new BinaryTreeNode<Roomie>(hab);

            //verifica si la puerta a crear debe estar cerrada o no
            //solo la primera puerta antes de un cuarto especial está cerrada
            if ((secciones == Secciones.L || secciones == Secciones.B) && (padre.Value.Seccion != secciones))
            {
                //indica la puerta cerrada
                estadoDoor = false;
                //Coloca distinción entre puerta del jefe a puerta de llave
                tipoPuerta = secciones == Secciones.B ? TipoBloque.Boss : TipoBloque.LockedDoor;
            }
            //añadir puertas al padre y al hijo
            if (padre.Value.PosX < coordX && padre.Value.PosY == coordY)
            {
                padre.Value.CrearPuerta(Direccion.Abajo, estadoDoor, tipoPuerta);
                hijo.Value.CrearPuerta(Direccion.Arriba, true, TipoBloque.Door);
            }
            else if (padre.Value.PosX > coordX && padre.Value.PosY == coordY) 
            {
                padre.Value.CrearPuerta(Direccion.Arriba, estadoDoor, tipoPuerta);
                hijo.Value.CrearPuerta(Direccion.Abajo, true, TipoBloque.Door);
            }
            else if (padre.Value.PosX == coordX && padre.Value.PosY > coordY)
            {
                padre.Value.CrearPuerta(Direccion.Izquierda, estadoDoor, tipoPuerta);
                hijo.Value.CrearPuerta(Direccion.Derecha, true, TipoBloque.Door);
            }
            else if (padre.Value.PosX == coordX && padre.Value.PosY < coordY)
            {
                padre.Value.CrearPuerta(Direccion.Derecha, estadoDoor, tipoPuerta);
                hijo.Value.CrearPuerta(Direccion.Izquierda, true, TipoBloque.Door);
            }

            //Se asigna el valor al hijo dependiendo por dnde haya tenido que bajar en el arreglo
            if (direccion == IZQUIERDA)
                padre.Left = hijo;
            else if(direccion == DERECHA)
                padre.Right = hijo;

            //añadir los elementos al arreglo
            _escenario[padre.Value.PosX, padre.Value.PosY] = padre.Value;
            _escenario[coordX, coordY] = hijo.Value;
        }

        /// <summary>
        /// Se recibe el padre de una sección en particular e inserta una habitación especial en
        /// en el final de uno de los pasillos de manera aleatoria
        /// </summary>
        /// <param name="padre"></param>
        /// <param name="secciones"></param>
        private void InsertarEspecialRoom(BinaryTreeNode<Roomie> padre, Secciones secciones)
        {
            //Insertar el cuarto correspondiente cuando no haya pasillo que recorrer
            if (padre.Left == null && padre.Right == null)
            {
                Secciones relleno;
                switch (secciones)
                {
                    case Secciones.K:
                        relleno = Secciones.TK;
                        break;
                    case Secciones.L:
                        relleno = Secciones.TT;
                        break;
                    case Secciones.B:
                        relleno = Secciones.TB;
                        break;
                    default:
                        relleno = Secciones.E;
                        break;
                }
                _escenario[padre.Value.PosX, padre.Value.PosY].Seccion = relleno;
                return;
            }
            //Si solo se puede avanzar por la izquierda, avanza por la izquierda
            else if (padre.Left != null && padre.Right == null)
                InsertarEspecialRoom(padre.Left, secciones);
            //Si solo se puede avanzar por la derecha, avanza por la izquierda
            else if (padre.Left == null && padre.Right != null)
                InsertarEspecialRoom(padre.Right, secciones);
            else //Si se puede avanzar por las dos, baja por una al azar
                InsertarEspecialRoom(_aleatorio.Next(0,2) == 0 ? padre.Right : padre.Right, secciones);
        }

        /// <summary>
        /// Busca si existe una posición adjacente disponible para crear una room
        /// </summary>
        /// <param name="room">Room de la cual se buscará si hay espacio para crear una habitación disponible</param>
        /// <param name="coordX">CoordX de la habitación disponible encontrada</param>
        /// <param name="coordY">CoordY de la habitación disponible encontrada</param>
        /// <returns>True: Hay espacio para crear una nueva habitación, False: Estacionado hay ningún espacio para crear una habitación adjacente</returns>
        private bool GetDisponible(BinaryTreeNode<Roomie> room, out int coordX, out int coordY)
        {
            //pares numericos de las cordenadas de las posiciones 
            //adjacentes al room actual
            int[,] pares = coordAdjacentes(room);
            //para que busque al azar a su vecino
            int[] lista = new int[4];
            //desordenar los valores de la lista
            lista = Aleatorio.DesordenarEnteros(lista);
            //verifica en toda la lista de pares si es posible crear una habitacion
            for (int i = 0; i < pares.GetLength(0); i++)
            {
                if (pares[lista[i], 0] > 0 && pares[lista[i], 1] > 0 && 
                    pares[lista[i], 0] < _escenario.GetLength(0) &&
                    pares[lista[i], 1] < _escenario.GetLength(1) &&
                    _escenario[pares[lista[i], 0], pares[lista[i], 1]].Seccion == Secciones.V)
                {
                    //asignar coordenadas x e y 
                    coordX = pares[lista[i], 0];
                    coordY = pares[lista[i], 1];
                    return true;
                }
            }
            //si después de recorrer la lista completa no encuetra un espacio disponible
            //retorna false y 0,0 a las coordenadas encontradas
            coordX = 0;
            coordY = 0;
            return false;
        }

        /// <summary>
        /// Retorna un arreglo con coordenadas adjacentes a un room dado
        /// </summary>
        /// <param name="room"> arreglo   4, 2 (4 pares de coordenadas)</param>
        /// <returns></returns>
        private int[,] coordAdjacentes(BinaryTreeNode<Roomie> room)
        {
            int[,] pares = new int[4, 2];
            pares[0, 0] = room.Value.PosX;
            pares[0, 1] = room.Value.PosY - 1;
            pares[1, 0] = room.Value.PosX + 1;
            pares[1, 1] = room.Value.PosY;
            pares[2, 0] = room.Value.PosX - 1;
            pares[2, 1] = room.Value.PosY;
            pares[3, 0] = room.Value.PosX;
            pares[3, 1] = room.Value.PosY + 1;
            return pares;
        }

        /// <summary>
        /// Crea un pasillo nuevo a partir de un pasillo anterior
        /// </summary>
        /// <param name="padre">Nodo padre del pasillo del que le saldrá el hijo</param>
        /// <param name="secciones">sección del pasillo hijo a crear</param>
        /// <param name="prof">profundidad del pasillo hijo</param>
        /// <returns>Retorna el nodo padre del árbol creado</returns>
        private BinaryTreeNode<Roomie> GenerarPasilloHijo(BinaryTreeNode<Roomie> padre, Secciones secciones, int prof)
        {
            int x, y;
            //para crear debe de haber una rama disponible y un espacio para construir
            if ((padre.Left == null || padre.Right == null) && GetDisponible(padre, out x, out y))
            {
                //Si el espacio izquierdo esta libre, lo usa
                if (padre.Left == null)
                {
                    CrearHabitacion(ref padre, IZQUIERDA, x, y, secciones);
                    GenerarPasilloLineal(padre.Left, secciones, prof);
                    return padre.Left;
                }
                else
                {
                    CrearHabitacion(ref padre, DERECHA, x, y, secciones);
                    GenerarPasilloLineal(padre.Right, secciones, prof);
                    return padre.Right;
                }
            }
            //en caso que las dos ramas estén ocupados baja por cualquiera de las dos
            else if (padre.Left != null && padre.Right != null)
            {
                return GenerarPasilloHijo(Aleatorio.Rng.Next(0, 2) == 0 ? padre.Left : padre.Right, secciones, prof);
            }
            else
            {
                //si hay algún brazo por donde bajar a pasar de no haber espacios disponibles
                if (padre.Left != null) return GenerarPasilloHijo(padre.Left, secciones, prof);
                if (padre.Right != null) return GenerarPasilloHijo(padre.Right, secciones, prof);
                //el PEOR de todos los casos posibles es que quede en una esquina sin salida, aquí deberá construir todo de nuevo random y rogar por que funcione xD
                Roomie[,] escenarioT = _escenario;
                GenerarDungeon();
                //Corar la recursividad de raíz 
                throw new ArgumentException("Error de creación, se tuvo que rehacer");
            }
        }
        #endregion

        #region DEBUG
        /// <summary>
        /// (DEBUG) Muestra el laberinto en la consola de pruebas, sirve para saber 
        /// cómo se está construyendo 
        /// </summary>
        /// <param name="_escenario"></param>
        [Conditional("DEBUG")]
        private void DrawDungeon()
        {
            string dibujo = string.Empty;
            Console.WriteLine("S = S, K = Key, Lock = L, B = B\n\n");
            for (int i = 0; i < _escenario.GetLength(0); i++)
            {
                for (int j = 0; j < _escenario.GetLength(1); j++)
                {
                    Console.BackgroundColor = ((int)_escenario[i, j].Seccion) == (int)Secciones.N ? ConsoleColor.Black : (ConsoleColor)_escenario[i, j].Seccion;
                    //Selecciona la grafica a mostrar de acuerdo al tipo de habitacion y/o seccion
                    switch ((int)_escenario[i, j].Seccion)
                    {
                        case 0:
                        case -1:
                            dibujo = " ";
                            break;
                        case (int)Secciones.TB:
                            dibujo = "@";
                            break;
                        case (int)Secciones.TK:
                            dibujo = "#";
                            break;
                        default:
                            dibujo = (_escenario[i, j].Seccion).ToString();
                            break;
                    }
                    Console.Write("{0} ", dibujo);
                }
                Console.WriteLine();
            }
            Console.ResetColor();
        } 
        #endregion
    }
}
