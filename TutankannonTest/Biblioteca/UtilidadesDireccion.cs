﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneracionProcedural
{
    /// <summary>
    /// Clase que reune las operaciones mas comunes sobre direcciones ortograficas, además
    /// sirve como Vector 2 para los proyectos que no estén asociados a MONOGAME
    /// </summary>
    public class UtilidadesDireccion
    {
        

        /// <summary>
        /// Retorna un vector de desplazamiento en el dungeon de acorde a la dirección
        /// especificada
        /// </summary>
        /// <param name="direccion">Dirección donde debiese moverse dentro del dungeon en x e y</param>
        /// <returns>vector 2 con el desplazamiento en x y en y</returns>
        public static Vector2 ObtenerDesplazamientoDireccion(Direccion direccion)
        {
            switch (direccion)
            {
                case Direccion.Arriba:
                    return new Vector2(-1, 0); // Arriba
                case Direccion.Derecha:
                    return new Vector2(0, +1); // Derecha
                case Direccion.Abajo:
                    return new Vector2(+1, 0); // Abajo
                case Direccion.Izquierda:
                    return new Vector2(0, -1);  // Izquierda
                default:
                    return new Vector2(0, 0);
            }
        }

        /// <summary>
        /// Retorna la dirección inversa a la que se está ingresando
        /// </summary>
        /// <param name="direccion">dirección ingresada</param>
        /// <returns>Dirección inversa a la ingresada</returns>
        public static Direccion InvertirDireccion(Direccion direccion) 
        {
            switch (direccion)
            {
                case Direccion.Arriba:
                    return Direccion.Abajo;
                case Direccion.Derecha:
                    return Direccion.Izquierda;
                case Direccion.Abajo:
                    return Direccion.Arriba;
                case Direccion.Izquierda:
                    return Direccion.Derecha;
                default:
                    return Direccion.Estacionado;
            }
        }
        
    }
}
