﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace GeneracionProcedural
{
    public class Vector2
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Vector2(int i, int j)
        {
            X = i;
            Y = j;
        }
    }
}
