﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneracionProcedural
{

    public enum Secciones
    {
        //S = Start, K = Key, L = Locked, B = Boss, N = Nada, TK = The Key, TB = The Boss, TT = The teaseure, Void = 0
        N = -1, V = 0, S = 1, K, L, B, TK, TB, TT, E
    }
}
