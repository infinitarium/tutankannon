﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneracionProcedural
{
    public enum TipoBloque
    {
        Void, Destructible, Destructible2, Indestructible, Key, Boss, Teasure, Door, LockedDoor, BossDoor, Traspasabe
    }
}