﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneracionProcedural.Tipos
{
    public enum EstadoPuerta
    {
       NO, Abierta, Cerrada
    }

    public enum PlayerPasa 
    {
        Si, No, Llave
    }
}
