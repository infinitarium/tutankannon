﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using GeneracionProcedural;

namespace Game1 
{
    class RenderTerreno:Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Enum que contiene todos los nombres de los modelos dentro de AllFBX
        #region Enum
        enum allFBX
        {
            arch_1_hue_1, arch_1_hue_2, arch_1_hue_3, cactus_01, cactus_02, cactus_03, cactus_04, cactus_05, cactus_06, cactus_07,
            cube_01_hue_1, cube_01_hue_2, cube_01_hue_3, cube_01_hue_4, cube_01_hue_5, cube_02_hue_1, cube_02_hue_2, cube_02_hue_3,
            cube_02_hue_4, cube_03_hue_1, cube_03_hue_2, cube_03_hue_3, cube_03_hue_4, cube_03_hue_5,pillar_large_segment_4_hue_3,
            ramp_concave_hue_1,ramp_concave_hue_2,ramp_concave_hue_3,ramp_concave_hue_4,ramp_concave_hue_5,ramp_convex_hue_1,
            ramp_convex_hue_2,ramp_convex_hue_3,ramp_convex_hue_4,ramp_convex_hue_5,ramp_straight_hue_1,ramp_straight_hue_2,
            ramp_straight_hue_3,ramp_straight_hue_4,ramp_straight_hue_5,rock_formation_01,rock_formation_02,rock_formation_03,
            rock_formation_04,rock_formation_05,rock_formation_06,rock_formation_07,rock_formation_08,rock_formation_09,rock_formation_10,
            rock_formation_11,rock_formation_12,rock_top_1x1,rock_top_2x2,rock_top_3x3,rock_top_4x4,rock_top_8x8,sample_level_entrance,
            sample_pyramid_entrance,sample_rock_formation,sand_top_1x1,sand_top_2x2,sand_top_3x3,sand_top_4x4,sand_top_8x8,shrub_01,shrub_02,
            shrub_03,shrub_04,shrub_05,shrub_06,shrub_07,shrub_08,skull_stone_01_hue_1,skull_stone_01_hue_2,skull_stone_01_hue_3,skull_stone_01_hue_4,
            skull_stone_01_hue_5,stair_corner_concave,stair_corner_concave_hue_1,stair_corner_concave_hue_2,stair_corner_concave_hue_3,
            stair_corner_concave_hue_4,stair_corner_convex,stair_corner_convex_hue_1,stair_corner_convex_hue_2,stair_corner_convex_hue_3,
            stair_corner_convex_hue_4,stair_sidewall,stair_sidewall_hue_1,stair_sidewall_hue_2,stair_sidewall_hue_3,stair_sidewall_hue_4,
            stair_straight_1,stair_straight_1_hue_1,stair_straight_1_hue_2,stair_straight_1_hue_3,stair_straight_1_hue_4,stair_straight_2,
            stair_straight_2_hue_1,stair_straight_2_hue_2,stair_straight_2_hue_3,stair_straight_2_hue_4,stairs_set_01,stairs_set_02,
            torch_big,torch_small,tumble_weed,wall_01,wall_02,wall_03,wall_04,wall_05,wall_06,wall_corner_concave_1,wall_corner_concave_2,
            wall_corner_convex_1,wall_corner_convex_2,wall_pannel_01_hue_1,wall_pannel_01_hue_2,wall_pannel_01_hue_3,wall_pannel_01_hue_4,
            wall_pannel_02_hue_1,wall_pannel_02_hue_2,wall_pannel_02_hue_3,wall_pannel_02_hue_4,wall_pannel_03_hue_1,wall_pannel_03_hue_2,
            wall_pannel_03_hue_3,wall_pannel_03_hue_4,wall_pannel_corner_hue_1,wall_pannel_corner_hue_2,wall_pannel_corner_hue_3,
            wall_pannel_corner_hue_4,wall_pillar_01,wall_pillar_02,cube_04_hue_1,cube_04_hue_2,cube_04_hue_3,cube_04_hue_4,cube_04_hue_5,
            cube_05_hue_1,cube_05_hue_2,cube_05_hue_3,cube_05_hue_4,cube_05_hue_5,cube_06_hue_1,cube_06_hue_2,cube_06_hue_3,cube_06_hue_4,
            cube_06_hue_5,cube_07_hue_1,cube_07_hue_2,cube_07_hue_3,cube_07_hue_4,cube_07_hue_5,cube_blue_01_hue_1,cube_blue_01_hue_2,
            cube_blue_01_hue_3,cube_blue_01_hue_4,deco_cow_skel_01,entrance_01,entrance_02,entrance_03,entrance_04,fence_01_broken_hue_1,
            fence_01_broken_hue_2,fence_01_broken_hue_3,fence_01_broken_hue_4,fence_01_half_hue_1,fence_01_half_hue_2,fence_01_half_hue_3,
            fence_01_half_hue_4,fence_01_hue_1,fence_01_hue_2,fence_01_hue_3,fence_01_hue_4,floor_switch_01,ground_block_1x1x1_dark,
            ground_block_1x1x1_light,ground_block_2x1x2,ground_block_2x2x2,ground_block_3x1x3,ground_block_3x2x3,ground_block_3x3x3,
            ground_block_4x1x4,ground_block_4x2x4,ground_block_4x4x4,ground_block_8x1x8,ground_block_8x2x8,ground_block_8x4x8,ground_block_8x8x8
            ,keyhole_blue,keyhole_gold,keyhole_red,ornament_01_hue_1,ornament_01_hue_2,ornament_01_hue_3,ornament_01_hue_4,ornament_01_small_hue_1
            ,ornament_01_small_hue_2,ornament_01_small_hue_3,ornament_01_small_hue_4,ornament_02_hue_1,ornament_02_hue_2,
            ornament_02_hue_3,ornament_02_hue_4,ornament_02_small_hue_1,ornament_02_small_hue_2,ornament_02_small_hue_3,ornament_02_small_hue_4,
            ornament_03_hue_1,ornament_03_hue_2,ornament_03_hue_3,ornament_03_hue_4,ornament_03_small_hue_1,ornament_03_small_hue_2,
            ornament_03_small_hue_3,ornament_03_small_hue_4,ornament_blue_01_hue_1,ornament_blue_01_hue_2,ornament_blue_01_hue_3,
            ornament_blue_01_small_hue_1,ornament_blue_01_small_hue_2,ornament_blue_01_small_hue_3,ornament_blue_02_hue_1,ornament_blue_02_hue_2,
            ornament_blue_02_hue_3,ornament_blue_02_small_hue_1,ornament_blue_02_small_hue_2,ornament_blue_02_small_hue_3,ornament_blue_03_hue_1,
            ornament_blue_03_hue_2,ornament_blue_03_hue_3,ornament_blue_03_small_hue_1,ornament_blue_03_small_hue_2,ornament_blue_03_small_hue_3,
            pedistol_01,pillar_1_hue_1,pillar_1_hue_2,pillar_1_hue_3,pillar_large_segment_1_hue_1,pillar_large_segment_1_hue_2,
            pillar_large_segment_1_hue_3,pillar_large_segment_1_hue_4,pillar_large_segment_1_hue_5,pillar_large_segment_1_hue_6,
            pillar_large_segment_2_hue_1,pillar_large_segment_2_hue_2,pillar_large_segment_2_hue_3,pillar_large_segment_2_hue_4,
            pillar_large_segment_2_hue_5,pillar_large_segment_3_hue_1,pillar_large_segment_3_hue_2,pillar_large_segment_3_hue_3,
            pillar_large_segment_3_hue_4,pillar_large_segment_4_hue_1,pillar_large_segment_4_hue_2
        }

        #endregion

        //Habitación
        Roomie room;

        //Camara
        Vector3 camTarget;
        Vector3 camPosition;
        Matrix projectionMatrix;
        Matrix viewMatrix;
        Matrix worldMatrix;

        //Modelos
        Model model, tanke;

        //Orbit
        bool orbit = false;

        public RenderTerreno()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

            //Setup Camera
            //Esta config es para ver al modelo completo al iniciar el programa
            camTarget = new Vector3(359.0f, 150.0f, 403.0f); //El objetivo a mirar de la cámara

            camPosition = new Vector3(463.199951f, 216.0f, 505.834473f); //La posición de la cámara


            //El cálculo de la proyección de la imagen desde los modelos hacia la cámara
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(
                               MathHelper.ToRadians(60f)//Campo de visión en grados
                               ,graphics.GraphicsDevice.Viewport.AspectRatio//Relación de aspecto de la ventana
                               ,1f       //El punto más cercano a renderizar
                               ,10000f   //El punto más lejano a renderizar
                               );
            viewMatrix = Matrix.CreateLookAt(camPosition, camTarget,
                         new Vector3(0f, 1f, 0f));// Y up
            worldMatrix = Matrix.CreateWorld(camTarget, Vector3.
                          Forward, Vector3.Up);
            //Se carga el modelo desde el Content Pipeline
            //El nombre del modelo con todos los meshes es AllWithTexturesFBX2013
            model = Content.Load<Model>("AllWithTexturesFBX2013");
            tanke = Content.Load<Model>("MonoTankeBinary");
            room = new Roomie();
            room.CrearRoom();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            // W       - Adelante
            // A       - Izquierda
            // S       - Atrás
            // D       - Derecha
            // C       - Bajar
            // Espacio - Subir
            #region Controles
            // Con Escape o el Back de un control de Xbox se sale de la app
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back ==
                ButtonState.Pressed || Keyboard.GetState().IsKeyDown(
                Keys.Escape))
                Exit();
            //Lo demás es movimiento de la cámara
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.X -= 10f;
                    camTarget.X -= 10f;
                }
                else
                {
                    camPosition.X -= 1.0f;
                    camTarget.X -= 1.0f;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.X += 10f;
                    camTarget.X += 10f;
                }
                else
                {
                    camPosition.X += 1f;
                    camTarget.X += 1f;
                }

            }
            if (Keyboard.GetState().IsKeyDown(Keys.C))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.Y -= 10f;
                    camTarget.Y -= 10f;
                }
                else
                {
                    camPosition.Y -= 1f;
                    camTarget.Y -= 1f;
                }

            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.Y += 10f;
                    camTarget.Y += 10f;
                }
                else
                {
                    camPosition.Y += 1f;
                    camTarget.Y += 1f;
                }

            }
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.Z -= 10f;
                    camTarget.Z -= 10f;
                }
                else
                {
                    camPosition.Z -= 1f;
                    camTarget.Z -= 1f;
                }

            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.Z += 10f;
                    camTarget.Z += 10f;
                }
                else
                {
                    camPosition.Z += 1f;
                    camTarget.Z += 1f;
                }

            }

            if (Keyboard.GetState().IsKeyDown(Keys.NumPad0))
            {
                orbit = !orbit;
            }

            if (orbit)
            {
                Matrix rotationMatrix = Matrix.CreateRotationY(
                                        MathHelper.ToRadians(1f));
                camPosition = Vector3.Transform(camPosition,
                              rotationMatrix);
            }
            viewMatrix = Matrix.CreateLookAt(camPosition, camTarget,
                         Vector3.Up); 
            #endregion

            //Con la R se pueden crear habitaciones aleatoriamente
            if (Keyboard.GetState().IsKeyDown(Keys.R))
            {
                room.CrearRoom();
            }


            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            #region Renderizar el modelo

            DibujarRoom(room);

            DrawTanke(new Vector3(100,25,100));


            #endregion

            base.Draw(gameTime);
        }

        /// <summary>
        /// Dibuja al tanque en una posición especifica
        /// </summary>
        /// <param name="vector3">El Vector3 que indica la posición en la que será creado el tanke</param>
        private void DrawTanke(Vector3 vector3)
        {
            Matrix tankWorldMatrix = Matrix.CreateScale(0.50f) * Matrix.CreateTranslation(vector3);
            foreach (ModelMesh mesh in tanke.Meshes)
            {

                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.LightingEnabled = true;
                    effect.AmbientLightColor = new Vector3(1.5f, 1.5f, 1.5f);// Una luz blanca tenue de ambiente
                    effect.DirectionalLight0.DiffuseColor = new Vector3(2.5f, 2.5f, 2.5f); // Una luz blanca más fuerte
                    effect.DirectionalLight0.Direction = new Vector3((-camPosition.X / 100), -1, 0); // Viniendo en diagonal desde los ejes de la cámara.
                    effect.DirectionalLight0.SpecularColor = new Vector3(0, 3, 0); // Con unos brillos pequeño que resaltan de color verde
                    effect.EmissiveColor = new Vector3(1, 1, 1);
                }
            }
            //Se dibuja el modelo completo, sin especificar ni filtrar.
            tanke.Draw(tankWorldMatrix, viewMatrix, projectionMatrix);
        }

        /// <summary>
        /// Dibuja un modelo específico en una posición entregada.
        /// </summary>
        /// <param name="modelPosition">El Vector3 que indica la posición en la que será creado el modelo a elegir</param>
        /// <param name="modelo">Un String que especifica el nombre del modelo que se quiere dibujar</param>
        private void DrawModelo(Vector3 modelPosition,string modelo)
        {

            foreach (var mesh in model.Meshes)
            {
                if (mesh.Name.Equals(modelo))
                {
                    foreach (BasicEffect effect in mesh.Effects)
                    {
                        
                        effect.LightingEnabled = true;
                        effect.AmbientLightColor = new Vector3(0.3f, 0.3f, 0.3f);// Una luz blanca de ambiente
                        effect.DirectionalLight0.DiffuseColor = new Vector3(0.5f, 0.5f, 0.5f); // Una luz blanca más fuerte
                        effect.DirectionalLight0.Direction = new Vector3((-camPosition.X / 100), -1, 0); // Viniendo en diagonal desde los ejes de la cámara.
                        effect.DirectionalLight0.SpecularColor = new Vector3(0.2f, 0.2f, 0.2f); // Con unos brillos pequeño que resaltan de color verde
                        effect.EmissiveColor = new Vector3(1, 1, 1);

                        effect.World = Matrix.CreateTranslation(modelPosition);
                        effect.View = viewMatrix;
                        effect.Projection = projectionMatrix;
                    }
                    // Con los efectos ya agregados, ya se puede dibujar el mesh
                    mesh.Draw();
                }
            }
        }

        /// <summary>
        /// Se esncarga de dibujar una habitación
        /// </summary>
        /// <param name="room"></param>
        private void DibujarRoom(Roomie room)
        {
            //Primera iteración para determinar el valor de X
            for (int i = 0; i < room.Room.GetLength(0); i++)
            {
                //Primera iteración para determinar el valor de Y
                for (int j = 0; j < room.Room.GetLength(1); j++)
                {
                    //Se dibuja un suelo
                    DrawModelo(new Vector3(i*100f,0f,j*100f),allFBX.sand_top_1x1.ToString());
                    //Se revisa si el room tiene bloques indestructibles o rompibles y los dibuja de acuerdo a su cualidad en el mapa
                    switch(room.Room[i, j].ToString())
                    {
                        case "Destructible": if (i != 2 && j != 2) DrawModelo(new Vector3(i * 100f, 10f, j * 100f), allFBX.ground_block_1x1x1_light.ToString());
                            break;
                        case "Indestructible": DrawModelo(new Vector3(i * 100f, 10f, j * 100f), allFBX.pillar_1_hue_1.ToString());
                            break;
                        default:
                            break;
                    }
                }
            }
        }

    }
}
