﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game1
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Camara
        Vector3 camTarget;
        Vector3 camPosition;
        Matrix projectionMatrix;
        Matrix viewMatrix;
        Matrix worldMatrix;

        //Geometric info
        Model model;

        //Orbit
        bool orbit = false;

        // camera settings
        private float cameraRotation = 0;
        private float cameraArc = 0;

        // mouse input used on Windows only
        MouseState mouse, mousePrev;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

            IsMouseVisible = true;
            //Setup Camera
            camTarget = new Vector3(0f, 0f, 0f); //Esta config es para ver al modelo completo al iniciar el programa
            camPosition = new Vector3(90f, 50f, 178f);
            //El cálculo de la proyección de la imagen desde los modelos hacia la cámara
            projectionMatrix = Matrix.CreatePerspectiveFieldOfView(
                               MathHelper.ToRadians(60f)//Campo de visión en grados
                               , graphics.GraphicsDevice.Viewport.AspectRatio//Relación de aspecto de la ventana
                               , 1f       //El punto más cercano a renderizar
                               , 10000f   //El punto más lejano a renderizar
                               );
            viewMatrix = Matrix.CreateLookAt(camPosition, camTarget,
                         new Vector3(0f, 1f, 0f));// Y up
            worldMatrix = Matrix.CreateWorld(camTarget, Vector3.
                          Forward, Vector3.Up);
            //Se carga el modelo desde el Content Pipeline
            model = Content.Load<Model>("MonoTankeSkinned");
            
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            // W       - Adelante
            // A       - Izquierda
            // S       - Atrás
            // D       - Derecha
            // C       - Bajar
            // Espacio - Subir
            #region Controles
            // Con Escape o el Back de un control de Xbox se sale de la app
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back ==
                ButtonState.Pressed || Keyboard.GetState().IsKeyDown(
                Keys.Escape))
                Exit();
            //Lo demás es movimiento de la cámara
            if (Keyboard.GetState().IsKeyDown(Keys.A))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.X -= 10f;
                    camTarget.X -= 10f;
                }
                else
                {
                    camPosition.X -= 1.0f;
                    camTarget.X -= 1.0f;
                }
            }
            if (Keyboard.GetState().IsKeyDown(Keys.D))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.X += 10f;
                    camTarget.X += 10f;
                }
                else
                {
                    camPosition.X += 1f;
                    camTarget.X += 1f;
                }

            }
            if (Keyboard.GetState().IsKeyDown(Keys.C))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.Y -= 10f;
                    camTarget.Y -= 10f;
                }
                else
                {
                    camPosition.Y -= 1f;
                    camTarget.Y -= 1f;
                }

            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.Y += 10f;
                    camTarget.Y += 10f;
                }
                else
                {
                    camPosition.Y += 1f;
                    camTarget.Y += 1f;
                }

            }
            if (Keyboard.GetState().IsKeyDown(Keys.W))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.Z -= 10f;
                    camTarget.Z -= 10f;
                }
                else
                {
                    camPosition.Z -= 1f;
                    camTarget.Z -= 1f;
                }

            }
            if (Keyboard.GetState().IsKeyDown(Keys.S))
            {
                if (Keyboard.GetState().IsKeyDown(Keys.LeftShift))
                {
                    camPosition.Z += 10f;
                    camTarget.Z += 10f;
                }
                else
                {
                    camPosition.Z += 1f;
                    camTarget.Z += 1f;
                }

            }

            if (Keyboard.GetState().IsKeyDown(Keys.NumPad0))
            {
                orbit = !orbit;
            }

            if (orbit)
            {
                Matrix rotationMatrix = Matrix.CreateRotationY(
                                        MathHelper.ToRadians(1f));
                camPosition = Vector3.Transform(camPosition,
                              rotationMatrix);
            }
            viewMatrix = Matrix.CreateRotationY(MathHelper.ToRadians(cameraRotation)) *
                          Matrix.CreateRotationX(MathHelper.ToRadians(cameraArc)) * Matrix.CreateLookAt(camPosition, camTarget,
                         Vector3.Up); 
            #endregion

            mousePrev = mouse;
            mouse = Mouse.GetState();

            if (mouse.LeftButton == ButtonState.Pressed)
            {
                HandleDrag(new Vector2(mouse.X - mousePrev.X, mouse.Y - mousePrev.Y));
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            //Setear los efectos de luces para cada mesh en el modelo
            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.LightingEnabled = true;
                    effect.AmbientLightColor = new Vector3(3, 3, 3);// Una luz blanca tenue de ambiente
                    effect.DirectionalLight0.DiffuseColor = new Vector3(5, 5, 5); // Una luz blanca más fuerte
                    effect.DirectionalLight0.Direction = new Vector3((-camPosition.X/100), -1, 0); // Viniendo en diagonal desde los ejes de la cámara.
                    effect.DirectionalLight0.SpecularColor = new Vector3(0, 3, 0); // Con unos brillos pequeño que resaltan de color verde
                    effect.EmissiveColor= new Vector3(1, 1, 1);
                }
            }

            foreach (ModelBone bone in model.Bones)
            {
                
            }
            //Se dibuja el modelo completo, sin especificar ni filtrar.
            model.Draw(worldMatrix,viewMatrix,projectionMatrix);
            base.Draw(gameTime);
        }

        private void HandleDrag(Vector2 delta)
        {
            cameraRotation += delta.X / 4;
            cameraArc = MathHelper.Clamp(cameraArc + delta.Y / 4, -70, 70);
        }



    }
}
