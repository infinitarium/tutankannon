﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Game1
{
    public class RenderTanke : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //Camara
        Vector3 camTarget;
        Vector3 camPosition;
        Matrix projectionMatrix;
        Matrix viewMatrix;
        Matrix worldMatrix;

        //Geometric info
        Model model;

        //Orbit
        bool orbit = false;

        public RenderTanke()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

            //Setup Camera
            camTarget = new Vector3(0f, 0f, 0f); //Esta config es para ver al modelo completo al iniciar el programa
            camPosition = new Vector3(90f, 50f, 178f);
            //projectionMatrix = Matrix.CreatePerspectiveFieldOfView(
            //                   MathHelper.ToRadians(55f), graphics.
            //                   GraphicsDevice.Viewport.AspectRatio,
            //    1f, 1000f);
            projectionMatrix = Matrix.CreateOrthographic(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height, 1.0f, 10000f); //Una proyección ortográfica :D
            viewMatrix = Matrix.CreateLookAt(camPosition, camTarget,
                         new Vector3(0f, 1f, 0f));// Y up
            worldMatrix = Matrix.CreateWorld(camTarget, Vector3.
                          Forward, Vector3.Up);
            //Se carga el modelo desde el Content Pipeline
            model = Content.Load<Model>("MonoTankeBinary");
            
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            // Con Escape o el Back de un control de Xbox se sale de la app
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back ==
                ButtonState.Pressed || Keyboard.GetState().IsKeyDown(
                Keys.Escape))
                Exit();
            //Lo demás es movimiento de la cámara
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                camPosition.X -= 0.1f;
                camTarget.X -= 0.1f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                camPosition.X += 0.1f;
                camTarget.X += 0.1f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                camPosition.Y -= 0.1f;
                camTarget.Y -= 0.1f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                camPosition.Y += 0.1f;
                camTarget.Y += 0.1f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.OemPlus))
            {
                camPosition.Z += 0.1f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.OemMinus))
            {
                camPosition.Z -= 0.1f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                orbit = !orbit;
            }

            if (orbit)
            {
                Matrix rotationMatrix = Matrix.CreateRotationY(
                                        MathHelper.ToRadians(1f));
                camPosition = Vector3.Transform(camPosition,
                              rotationMatrix);
            }
            viewMatrix = Matrix.CreateLookAt(camPosition, camTarget,
                         Vector3.Up);

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            //Setear los efectos de luces para cada mesh en el modelo
            foreach (ModelMesh mesh in model.Meshes)
            {
                
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.LightingEnabled = true;
                    effect.AmbientLightColor = new Vector3(3, 3, 3);// Una luz blanca tenue de ambiente
                    effect.DirectionalLight0.DiffuseColor = new Vector3(5, 5, 5); // Una luz blanca más fuerte
                    effect.DirectionalLight0.Direction = new Vector3((-camPosition.X/100), -1, 0); // Viniendo en diagonal desde los ejes de la cámara.
                    effect.DirectionalLight0.SpecularColor = new Vector3(0, 3, 0); // Con unos brillos pequeño que resaltan de color verde
                    effect.EmissiveColor= new Vector3(1, 1, 1);
                }
            }
            //Se dibuja el modelo completo, sin especificar ni filtrar.
            model.Draw(worldMatrix,viewMatrix,projectionMatrix);
            base.Draw(gameTime);
        }
    }
}

