﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneracionProcedural
{
    public static class Aleatorio
    {
        private static Random _rng;

        /// <summary>
        /// Genera u numero aleatorio persistente. En C# cada vez que se crea un 
        /// new Random() comienza con la misma secuencia de números, por eso en este caso estoy usando una de tipo static que se 
        /// inicializa una vez en toda la solución
        /// </summary>
        public static Random Rng
        {
            get 
            {
                if (_rng == null)
                {
                    _rng = new Random();
                }
                return _rng; 
            }
            set { _rng = value; }
        }

        /// <summary>
        /// Desordena un arreglo de numeros recibido
        /// </summary>
        /// <param name="lista">lista de numeros enteros</param>
        /// <returns>misma lista pero con valores desordenados</returns>
        public static int[] DesordenarEnteros(int[] lista)
        {
            //crea un arreglo de numeros consecutivos 
            for (int i = 0; i < lista.Length; i++)
                lista[i] = i;
            //desordena el arreglo para que comience a verificar cuartos 
            //adjacentes de manera aleatoria y así entregar variedad al _escenario
            for (int i = 0; i < lista.Length; i++)
            {
                int cambio = _rng.Next(0, 4);
                int temporal = lista[i];
                lista[i] = lista[cambio];
                lista[cambio] = temporal;
            }
            return lista;
        }
        
    }
}
