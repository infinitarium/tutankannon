﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeneracionProcedural.Tipos;

namespace GeneracionProcedural
{
    /// <summary>
    /// Nodo que representa una habitacion
    /// </summary>
    public class Roomie
    {
        #region Campos y Propiedades
        private const int X = 0, Y = 1;
        public int PosX { get; set; }
        public int PosY { get; set; }
        private TipoBloque[,] _room;
        private int[,] _doors;
        private EstadoPuerta[] _doorsPosition;
        private Random _aleatorio;
        private Secciones _seccion;

        public Secciones Seccion
        {
            get { return _seccion; }
            set 
            {
                //si es habitacion de llave, dibuja una llave
                if (value == Secciones.TK)
                    _room[(_room.GetLength(0) - 1) / 2, (_room.GetLength(1) - 1) / 2] = TipoBloque.Key;
                else if (value == Secciones.TB)
                    _room[(_room.GetLength(0) - 1) / 2, (_room.GetLength(1) - 1) / 2] = TipoBloque.Boss;
                else if (value == Secciones.TT)
                    _room[(_room.GetLength(0) - 1) / 2, (_room.GetLength(1) - 1) / 2] = TipoBloque.Teasure;
                //Si la llave/jefe/tesoro es tomado(a), se saca del escenario
                else if ((_seccion == Secciones.TB || _seccion == Secciones.TK || _seccion == Secciones.TT))
                {
                    _room[(_room.GetLength(0) - 1) / 2, (_room.GetLength(1) - 1) / 2] = TipoBloque.Void;
                }
                _seccion = value;
            }
        }

        public EstadoPuerta[] Doors
        {
            get { return _doorsPosition; }
        }
        
        public TipoBloque[,] Room
        {
            get { return _room; }
        } 
        #endregion

        #region Constructor e Inicializador
        public Roomie()
        {
            Initialize();
        }
       
        /// <summary>
        /// Método inializador del rooom
        /// </summary>
        private void Initialize()
        {
            _aleatorio = Aleatorio.Rng;
            PosX = 0;
            PosY = 0;
            CrearRoom();
            //incializar puertas
            _doors = new int[4, 2];
            _doorsPosition = new EstadoPuerta[4];
            for (int i = 0; i < _doorsPosition.Length; i++)
            {
                _doorsPosition[i] = EstadoPuerta.NO;
            }
        }
        #endregion

        public void CrearPuerta(Direccion direccion, bool abierta, TipoBloque tipoPuerta) 
        {
            //puertas por defecto asignadas una por una 
            //Door UP
            _doors[3, X] = _room.GetLength(0) / 2;
            _doors[3, Y] = 0;
            //Door RIGHT
            _doors[2, X] = _room.GetLength(0) - 1;
            _doors[2, Y] = _room.GetLength(1) / 2;
            //Door down
            _doors[1, X] = _room.GetLength(0) / 2;
            _doors[1, Y] = _room.GetLength(1) - 1;
            //Door LEFT
            _doors[0, X] = 0;
            _doors[0, Y] = _room.GetLength(1) / 2;
            //Se agrega la puerta dependiendo de su posicion y tamañp del room
            _room[_doors[(int)direccion, X], _doors[(int)direccion, Y]] = tipoPuerta;
            //Indica que hay una puerta en la dirección dada
            _doorsPosition[(int)direccion] = abierta ? EstadoPuerta.Abierta : EstadoPuerta.Cerrada; 
        }

        /// <summary>
        /// Crea una habitacion (arreglo room) estilo Bomberman
        /// </summary>
        public void CrearRoom()
        {
            _room = new TipoBloque[_aleatorio.Next(8, 20), _aleatorio.Next(8, 30)];
            //recorrer todas las filas y columnas
            for (int i = 0; i < _room.GetLength(0); i++)
            {
                for (int j = 0; j < _room.GetLength(1); j++)
                {
                    //rellena con bloques indestructible en los bordes
                    _room[i,j] = (i == 0 || i == _room.GetLength(0) - 1 || j == 0 || j == _room.GetLength(1) - 1 ?
                        TipoBloque.Indestructible :
                            //Agrega bloques destruibles en las posiciones impares    
                                (i % 2 == 0) && (j % 2 == 0) ?
                                TipoBloque.Destructible :
                                    //hay una probabilidad de un x% de agregar un nuevo bloque destruible entre dos destruibles
                                    (_aleatorio.Next(0, 5) == 0) ?
                                    TipoBloque.Destructible : TipoBloque.Void);
                }
            }   
        }

    }
}
