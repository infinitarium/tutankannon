﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GeneracionProcedural
{
    public class Vector2Maze
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Vector2Maze(int i, int j)
        {
            X = i;
            Y = j;
        }

        /// <summary>
        /// Retorna un vector de desplazamiento en el dungeon de acorde a la dirección
        /// especificada
        /// </summary>
        /// <param name="direccion">Dirección donde debiese moverse dentro del dungeon en x e y</param>
        /// <returns>vector 2 con el desplazamiento en x y en y</returns>
        public static Vector2Maze DesplazamientoDireccion(Direccion direccion)
        {
            switch (direccion)
            {
                case Direccion.Arriba:
                    return new Vector2Maze(-1, 0); // Arriba
                case Direccion.Derecha:
                    return new Vector2Maze(0, +1); // Derecha
                case Direccion.Abajo:
                    return new Vector2Maze(+1, 0); // Abajo
                case Direccion.Izquierda:
                    return new Vector2Maze(0, -1);  // Izquierda
                default:
                    return new Vector2Maze(0, 0);
            }
        }
        
    }
}
