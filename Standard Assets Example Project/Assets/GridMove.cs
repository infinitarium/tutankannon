﻿using System.Collections;
using UnityEngine;

class GridMove : MonoBehaviour
{
    private float moveSpeed = 3f;
    private float gridSize = 1f;
    public RoomManager manager;
    private enum Orientation
    {
        Horizontal,
        Vertical
    };
    private Orientation gridOrientation = Orientation.Horizontal;
    private bool allowDiagonals = false;
    private bool correctDiagonalSpeed = true;
    private Vector2 input;
    private float factor;
    [HideInInspector]
    public float t;
    public bool isMoving = false;
    public Vector3 startPosition;
    public Vector3 endPosition;

    public void Update()
    {
        if (!isMoving)
        {
            input = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            if (!allowDiagonals)
            {
                if (Mathf.Abs(input.x) > Mathf.Abs(input.y))
                {
                    input.y = 0;
                }
                else {
                    input.x = 0;
                }
            }

            if (input != Vector2.zero)
            {
                StartCoroutine(move(transform));
            }
        }
    }

    public IEnumerator move(Transform transform)
    {
        isMoving = true;
        startPosition = transform.position;
        t = 0;

        if (gridOrientation == Orientation.Horizontal)
        {
            endPosition = new Vector3(startPosition.x + System.Math.Sign(input.x) * gridSize,
                startPosition.y, startPosition.z + System.Math.Sign(input.y) * gridSize);
        }
        else {
            endPosition = new Vector3(startPosition.x + System.Math.Sign(input.x) * gridSize,
                startPosition.y + System.Math.Sign(input.y) * gridSize, startPosition.z);
        }

        if (allowDiagonals && correctDiagonalSpeed && input.x != 0 && input.y != 0)
        {
            factor = 0.7071f;
        }
        else {
            factor = 1f;
        }
        if (manager.getRoom()[(int)endPosition.x,(int)endPosition.z].ToString().Equals("Void"))
        {
            if (input.x > 0f)
            {
                while (t < 1f)
                {
					if (Input.GetKey (KeyCode.LeftShift)) {
						t += Time.deltaTime * (moveSpeed / gridSize) * factor;
						transform.rotation = Quaternion.Lerp (Quaternion.AngleAxis (transform.rotation.eulerAngles.y, Vector3.up), Quaternion.AngleAxis (90f, Vector3.up), t);
						yield return null;
					} else {
						t += Time.deltaTime * (moveSpeed / gridSize) * factor;
						transform.rotation = Quaternion.Lerp (Quaternion.AngleAxis (transform.rotation.eulerAngles.y, Vector3.up), Quaternion.AngleAxis (90f, Vector3.up), t);
						transform.position = Vector3.Lerp (startPosition, endPosition, t);
						yield return null;
					}
                }
            }
            else if (input.x < 0f)
            {
                while (t < 1f)
                {
					if (Input.GetKey (KeyCode.LeftShift)) {
						t += Time.deltaTime * (moveSpeed / gridSize) * factor;
						transform.rotation = Quaternion.Lerp(Quaternion.AngleAxis(transform.rotation.eulerAngles.y, Vector3.up), Quaternion.AngleAxis(270f, Vector3.up), t);
						yield return null;
					} else {
						t += Time.deltaTime * (moveSpeed / gridSize) * factor;
						transform.rotation = Quaternion.Lerp(Quaternion.AngleAxis(transform.rotation.eulerAngles.y, Vector3.up), Quaternion.AngleAxis(270f, Vector3.up), t);
						transform.position = Vector3.Lerp(startPosition, endPosition, t);
						yield return null;
					}
                }
            }
            if (input.y > 0f)
            {
                while (t < 1f)
                {
					if (Input.GetKey (KeyCode.LeftShift)) {
						t += Time.deltaTime * (moveSpeed / gridSize) * factor;
						transform.rotation = Quaternion.Lerp (Quaternion.AngleAxis (transform.rotation.eulerAngles.y, Vector3.up), Quaternion.AngleAxis (0f, Vector3.up), t);
						yield return null;
					} else {
						t += Time.deltaTime * (moveSpeed / gridSize) * factor;
						transform.rotation = Quaternion.Lerp (Quaternion.AngleAxis (transform.rotation.eulerAngles.y, Vector3.up), Quaternion.AngleAxis (0f, Vector3.up), t);
						transform.position = Vector3.Lerp (startPosition, endPosition, t);
						yield return null;
					}
                }
            }
            else if (input.y < 0f)
            {
                while (t < 1f)
                {
					if (Input.GetKey (KeyCode.LeftShift)) {
						t += Time.deltaTime * (moveSpeed / gridSize) * factor;
						transform.rotation = Quaternion.Lerp (Quaternion.AngleAxis (transform.rotation.eulerAngles.y, Vector3.up), Quaternion.AngleAxis (180f, Vector3.up), t);
						yield return null;
					} else {
						t += Time.deltaTime * (moveSpeed / gridSize) * factor;
						transform.rotation = Quaternion.Lerp (Quaternion.AngleAxis (transform.rotation.eulerAngles.y, Vector3.up), Quaternion.AngleAxis (180f, Vector3.up), t);
						transform.position = Vector3.Lerp (startPosition, endPosition, t);
						yield return null;
					}
                }
            }

        }

        isMoving = false;
        yield return 0;
    }
}