﻿using UnityEngine;
using System.Collections;

public class Disparar : MonoBehaviour {

	public GameObject prefabBala;
	public Transform posInicial;
	public float coolDownDisparos = 1f;
	public float timeStamp;
	private GameObject balita;
	private GridMove conPla;

	// Use this for initialization
	void Start () {
		timeStamp = Time.time;
		balita = prefabBala;
		conPla = GetComponent<GridMove>();
	}
	
	// Update is called once per frame
	void Update(){
	}

	void FixedUpdate () {
		if (Input.GetButton("Fire1")) {
			Shoot ();
		}
	}

	public void Shoot(){
		if (timeStamp <= Time.time && !conPla.isMoving) {
			balita = Instantiate (prefabBala, posInicial.position, posInicial.rotation) as GameObject;
			timeStamp = Time.time + coolDownDisparos;
		}
	}
}