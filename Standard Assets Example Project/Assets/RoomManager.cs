﻿using UnityEngine;
using System.Collections;
using System;

public class RoomManager: MonoBehaviour {

    public enum TipoBloque
    {
        Destructible,Indestructible,Void
    }

    public GameObject m_TankPrefab;
    public GameObject m_Wall;
    public GameObject m_Destructable;
    public GameObject m_Indestructible;
    public GameObject m_Enemy;
    public int cantidadEnemy;
    public int EnemyRoom;
    private TipoBloque[,] _room;
    private System.Random _aleatorio;
    private bool isPlayerSpawned = false;
    private bool isEnemyEnough = false;
    private RoomManager manager;
    private GridMove movimiento;

    // Use this for initialization
    void Start () {

        _aleatorio = new System.Random();
        CrearRoom();
        cantidadEnemy = 0;
        EnemyRoom = 3;
	
	}

    private void CrearRoom()
    {
        _room = new TipoBloque[_aleatorio.Next(8, 20), _aleatorio.Next(8, 30)];
        //recorrer todas las filas y columnas
        for (int i = 0; i < _room.GetLength(0); i++)
        {
            for (int j = 0; j < _room.GetLength(1); j++)
            {
                //rellena con bloques indestructible en los bordes
                _room[i, j] = (i == 0 || i == _room.GetLength(0) - 1 || j == 0 || j == _room.GetLength(1) - 1 ?
                    TipoBloque.Indestructible :
                            //Agrega bloques destruibles en las posiciones impares    
                            (i % 2 == 0) && (j % 2 == 0) ?
                            TipoBloque.Destructible :
                                //hay una probabilidad de un x% de agregar un nuevo bloque destruible entre dos destruibles
                                (_aleatorio.Next(0, 5) == 0) ?
                                TipoBloque.Destructible : TipoBloque.Void);
            }
        }


        DrawRoom();
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            LimpiarEscena();
            CrearRoom();
        }
	}

    private void LimpiarEscena()
    {
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Escena"))
        {
            Destroy(item);
        }
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        Destroy(GameObject.FindGameObjectWithTag("ThirdPersonController"));
        isPlayerSpawned = false;
    }

    private void DrawRoom()
    {
        for (int i = 0; i < _room.GetLength(0); i++)
        {
            for (int j = 0; j < _room.GetLength(1); j++)
            {
                m_Indestructible = Instantiate(m_Indestructible);
                float Y = m_Indestructible.transform.position.y;
                m_Indestructible.transform.position = new Vector3(i * 1f, Y, j * 1f);
                switch (_room[i,j].ToString())
                {
                    case "Destructible":
                        m_Destructable = Instantiate(m_Destructable);
                        Y = m_Destructable.transform.position.y;
                        m_Destructable.transform.position = new Vector3(i * 1f, Y, j * 1f);
                        break;
                    case "Indestructible":
                        m_Wall = Instantiate(m_Wall);
                        Y = m_Wall.transform.position.y;
                        m_Wall.transform.position = new Vector3(i * 1f, Y, j * 1f);
                        break;
                    case "Void":
                        if(!isPlayerSpawned)ColocarPlayer();
                        if (!isEnemyEnough) ColocarEnemy();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void ColocarPlayer()
    {
        int i = 0;
        int j = 0;
        for (i=_aleatorio.Next(_room.GetLength(0)-1); !isPlayerSpawned; i++)
        {
            for (j=_aleatorio.Next(_room.GetLength(1)-1); !isPlayerSpawned; j++)
            {
                if (_room[i,j].ToString().Equals("Void"))
                {
                    m_TankPrefab = Instantiate(m_TankPrefab, new Vector3(i * 1f, m_TankPrefab.transform.position.y, j * 1f), Quaternion.identity) as GameObject;
                    //float Y = m_TankPrefab.transform.position.y;
                    //m_TankPrefab.transform.position = new Vector3(i * 1f, Y, j * 1f);
                    manager = gameObject.GetComponent<RoomManager>();
                    movimiento = m_TankPrefab.GetComponent<GridMove>();
                    movimiento.manager = manager;
                    isPlayerSpawned = true;
                    GameObject.Find("Camera").GetComponent<ControlCamara>().player = m_TankPrefab;

                }
                else
                {
                    i = _aleatorio.Next(_room.GetLength(0));
                    j = _aleatorio.Next(_room.GetLength(1));
                }
            }
        }
                
    }

    private void ColocarEnemy()
    {
        int i = 0;
        int j = 0;
        for (i = _aleatorio.Next(_room.GetLength(0) - 1); !isEnemyEnough; i++)
        {
            for (j = _aleatorio.Next(_room.GetLength(1) - 1); !isEnemyEnough; j++)
            {
                if (_room[i, j].ToString().Equals("Void"))
                {
                    m_Enemy = Instantiate(m_Enemy, new Vector3(i * 1f, m_Enemy.transform.position.y, j * 1f), Quaternion.identity) as GameObject;
                    manager = gameObject.GetComponent<RoomManager>();
                    movimiento.manager = manager;
                    cantidadEnemy = cantidadEnemy + 1;
                    if(cantidadEnemy == EnemyRoom)
                    {
                        isEnemyEnough = true;
                    }
                }
                else
                {
                    i = _aleatorio.Next(_room.GetLength(0));
                    j = _aleatorio.Next(_room.GetLength(1));
                }
            }
        }

    }


    public TipoBloque[,] getRoom()
    {
        return _room;
    }
}
