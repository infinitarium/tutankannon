﻿using UnityEngine;
using System.Collections;
using System;

public class ControlCamara : MonoBehaviour {

    // Use this for initialization
    public int AltitudeAway = 10;
    public float cameraDistOffset = 10;
    public float velCamara=0.1f;
    private Camera mainCamera;
    public GameObject player;
    private GridMove conPla;
    private bool isMoving;
    void Awake()
    {
		
    }

    // Use this for initialization
    void Start()
    {
        System.Threading.Thread.Sleep(2000);
        
        transform.LookAt(player.transform.position);
        transform.rotation = Quaternion.AngleAxis(65f, Vector3.right);
        transform.position = new Vector3(player.transform.position.x, player.transform.position.y + AltitudeAway, player.transform.position.z - cameraDistOffset);
        conPla = player.GetComponent<GridMove>();
    }

    // Update is called once per frame
    void Update()
    {
        if (conPla.isMoving)
        {
            Vector3 posInic = transform.position;
            Vector3 posObjetivo = conPla.endPosition;
            Vector3 posNueva = new Vector3(posObjetivo.x, posObjetivo.y + AltitudeAway, posObjetivo.z - cameraDistOffset);
            while (conPla.t<1)
            {
                transform.position = Vector3.Lerp(posInic, posNueva, conPla.t);
            }
        }
        else
        {
            transform.position = new Vector3(player.transform.position.x, player.transform.position.y + AltitudeAway, player.transform.position.z - cameraDistOffset);
        }
    }

}
