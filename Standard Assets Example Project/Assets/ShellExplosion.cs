﻿using UnityEngine;
using System.Collections;

public class ShellExplosion : MonoBehaviour {
	
    public float m_MaxLifeTime = 5f;                    // The time in seconds before the shell is removed.
	public float m_ExplosionRadius = 5f;                // The maximum distance away from the explosion tanks can be and are still affected.
	public float velocidad = 5f;


    private void Start()
    {
        // If it isn't destroyed by then, destroy the shell after it's lifetime.
        Destroy(gameObject, m_MaxLifeTime);
    }

	private void Update(){
		transform.Translate(Vector3.forward * Time.deltaTime * velocidad);
	}

	/*
    private void OnTriggerEnter(Collider other)
    {
        // Collect all the colliders in a sphere from the shell's current position to a radius of the explosion radius.
        Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_EnemyMask);

        // Go through all the colliders...
        for (int i = 0; i < colliders.Length; i++)
        {
            // ... and find their rigidbody.
            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

            // If they don't have a rigidbody, go on to the next collider.
            if (!targetRigidbody)
                continue;

            Destroy(targetRigidbody.GetComponentInParent<GameObject>());
            
        }
       
        // Destroy the shell.
        Destroy(gameObject);
    }*/

	private void OnTriggerEnter(Collider otro)
	{
		Destroy (gameObject);
		Destroy (otro.gameObject);
	}

}
