﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EjemploParticulas
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    static class Program
    {
        static void Main()
        {
            using (ParticleSampleGame game = new ParticleSampleGame())
            {
                game.Run();
            }
        }
    }
#endif
}
