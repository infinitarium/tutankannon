﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace SimpleMenu
{
    class Audio
    {
        List<Song> song;
        List<SoundEffect> soundEffects;

        public Audio(ContentManager content)
        {
            song = new List<Song>();
            song.Add(content.Load<Song>("Songs/Frozen_Star"));
            song.Add(content.Load<Song>("Songs/Belief"));
            soundEffects = new List<SoundEffect>();
            soundEffects.Add(content.Load<SoundEffect>("heavy_stream_of_water_in_cave"));
            soundEffects.Add(content.Load<SoundEffect>("metal_weight_plate_dropped"));
        }

        public void LoadContent(ContentManager content)
        {
            MediaPlayer.Play(song.Find(x => x.Name == "Frozen_Star"));
            MediaPlayer.Volume = 0.6f;           
        }

        public void Update(GameTime gameTime)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.D1))
                soundEffects[0].CreateInstance().Play();
            if (Keyboard.GetState().IsKeyDown(Keys.D2))
                soundEffects[1].CreateInstance().Play();
            if (Keyboard.GetState().IsKeyDown(Keys.Space))
            {
                if (SoundEffect.MasterVolume == 0.0f)
                    SoundEffect.MasterVolume = 1.0f;
                else
                    SoundEffect.MasterVolume = 0.0f;
            }

            if(Keyboard.GetState().IsKeyDown(Keys.M))
            {
                BotonMute();
            }
        }

        public void BotonMute()
        {
            if (MediaPlayer.Volume == 0.0f)
            {
                MediaPlayer.Volume = 0.3f;
            }
            else
            {
                MediaPlayer.Volume = 0.0f;
            }
        }

        public void CambiarTrack(string nombre)
        {
            MediaPlayer.Stop();
            MediaPlayer.Play(song.Find(x => x.Name == "Belief"));
            MediaPlayer.Volume = 0.6f;
        }
    }
}
