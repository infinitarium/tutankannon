﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleMenu
{
    class MainMenu
    {
        enum GameState {loading ,mainMenu, enterName, opcion, InGame};
        enum GameAudio { sonido , mute};

        GameState gameState;
        GameAudio gameAudio;
        Audio audioClass;
        public bool graphics;
        List<GUIElement> loadingScreen = new List<GUIElement>();
        List<GUIElement> audio = new List<GUIElement>();
        List<GUIElement> main = new List<GUIElement>();
        List<GUIElement> enterName = new List<GUIElement>();
        List<GUIElement> opcion = new List<GUIElement>();

        private Keys[] lastPressedKeys = new Keys[5];

        private string myName = "";
        private SpriteFont spriteFont;

        public MainMenu()
        {
            loadingScreen.Add(new GUIElement("loading_screen"));
            main.Add(new GUIElement("MenuBtn/menu"));
            main.Add(new GUIElement("MenuBtn/newGame"));
            main.Add(new GUIElement("MenuBtn/loadGame"));
            main.Add(new GUIElement("MenuBtn/option"));

            enterName.Add(new GUIElement("MenuBtn/name"));
            enterName.Add(new GUIElement("MenuBtn/done"));
            enterName.Add(new GUIElement("MenuBtn/back"));

            audio.Add(new GUIElement("sound"));
            audio.Add(new GUIElement("mute"));

            
            opcion.Add(new GUIElement("MenuBtn/menu"));
            opcion.Add(new GUIElement("MenuBtn/back"));
            opcion.Add(new GUIElement("MenuBtn/fullScreen"));
        }

        public void LoadContent(ContentManager content)
        {
            spriteFont = content.Load<SpriteFont>("MenuBtn/MyFont");
            audioClass = new Audio(content);
            graphics = true;
            foreach (GUIElement element in loadingScreen)
            {
                element.LoadContent(content);
                element.CenterElement(GraphicsDeviceManager.DefaultBackBufferWidth, GraphicsDeviceManager.DefaultBackBufferHeight*3);
                element.clickEvent += OnClick;
            }
            foreach (GUIElement element in main)
            {
                element.LoadContent(content);
                element.CenterElement(GraphicsDeviceManager.DefaultBackBufferWidth, GraphicsDeviceManager.DefaultBackBufferHeight * 3);
                element.clickEvent += OnClick;
            }
            main.Find(x => x.AssetName == "MenuBtn/newGame").MoveElement(0, -100);
            main.Find(x => x.AssetName == "MenuBtn/option").MoveElement(0, 100);

            foreach (GUIElement element in enterName)
            {
                element.LoadContent(content);
                element.CenterElement(GraphicsDeviceManager.DefaultBackBufferWidth, GraphicsDeviceManager.DefaultBackBufferHeight * 3);
                element.clickEvent += OnClick;
            }
            enterName.Find(x => x.AssetName == "MenuBtn/done").MoveElement(50, 60);
            enterName.Find(x => x.AssetName == "MenuBtn/back").MoveElement(-50, 60);
            foreach (GUIElement element in audio)
            {
                element.LoadContent(content);
                element.CenterElement(GraphicsDeviceManager.DefaultBackBufferWidth, GraphicsDeviceManager.DefaultBackBufferHeight * 3);
                element.clickEvent += OnClick;
            }
            audio.Find(x => x.AssetName == "sound").MoveElement(-350, 150);
            audio.Find(x => x.AssetName == "mute").MoveElement(-350, 150);

            foreach (GUIElement element in opcion)
            {
                element.LoadContent(content);
                element.CenterElement(GraphicsDeviceManager.DefaultBackBufferWidth, GraphicsDeviceManager.DefaultBackBufferHeight * 3);
                element.clickEvent += OnClick;
            }
            opcion.Find(x => x.AssetName == "MenuBtn/back").MoveElement(0, 60);
            
        }

        public void Update()
        {
            switch(gameState)
            {
                case GameState.enterName:
                    foreach (GUIElement element in enterName)
                    {
                        element.Update();
                    }
                    GetKeys();
                    break;
                case GameState.InGame:
                    audioClass.CambiarTrack("Belief");
                    break;
                case GameState.mainMenu:
                    foreach (GUIElement element in main)
                    {
                        element.Update();
                    }
                    break;
                case GameState.loading:
                    foreach (GUIElement element in loadingScreen)
                    {
                        element.Update();
                    }
                    break;
                case GameState.opcion:
                    foreach (GUIElement element in opcion)
                    {
                        element.Update();
                    }
                    break;
            }
            switch(gameAudio)
            {
                case GameAudio.sonido:
                    audio.Find(x => x.AssetName == "sound").Update();
                    break;
                case GameAudio.mute:
                    audio.Find(x => x.AssetName == "mute").Update();
                    break;
            }
            
            
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            switch(gameState)
           {
                case GameState.enterName:
                    foreach (GUIElement element in enterName)
                    {
                        element.Draw(spriteBatch);
                    }
                    spriteBatch.DrawString(spriteFont, myName, new Vector2(305, 300), Color.Black);
                    break;
                case GameState.InGame:
                    break;
                case GameState.mainMenu:
                    foreach (GUIElement element in main)
                     {
                        element.Draw(spriteBatch);
                     }
                    break;
                case GameState.loading:
                    foreach (GUIElement element in loadingScreen)
                    {
                        element.Draw(spriteBatch);
                    }
                    break;
                case GameState.opcion:
                    foreach (GUIElement element in opcion)
                    {
                        element.Draw(spriteBatch);
                    }
                    break;
            }
            switch (gameAudio)
            {
                case GameAudio.sonido:
                    audio.Find(x => x.AssetName == "sound").Draw(spriteBatch);
                    break;
                case GameAudio.mute:
                    audio.Find(x => x.AssetName == "mute").Draw(spriteBatch);
                    break;
            }
            
        }

        public void OnClick(string element)
        {
            if (element == "loading_screen")
            {
                gameState = GameState.mainMenu;
            }              
            if(element == "MenuBtn/newGame")
            {
                gameState = GameState.enterName;;
            }
            if(element == "MenuBtn/back")
            {
                gameState = GameState.mainMenu;
            }
            if(element == "MenuBtn/done")
            {             
                gameState = GameState.InGame;
            }
            if(element == "sound")
            {
                gameAudio = GameAudio.mute;
                audioClass.BotonMute();            
            }
            if(element == "mute")
            {
                gameAudio = GameAudio.sonido;
                audioClass.BotonMute();
            }
            if (element == "MenuBtn/option")
            {
                gameState = GameState.opcion;
            }
            if (element == "MenuBtn/fullScreen")
            {
                if (graphics == true)
                {
                    graphics = false;
                }
                else
                {
                    graphics = false;
                }
            }
        }

        public void GetKeys()
        {
            KeyboardState kbState = Keyboard.GetState();
            Keys[] pressedKeys = kbState.GetPressedKeys();

            foreach (Keys key in lastPressedKeys)
            {
                if(!pressedKeys.Contains(key))
                {
                    OnKeyUp(key);
                }
            }
            foreach (Keys key in pressedKeys)
            {
                if(!lastPressedKeys.Contains(key))
                {
                    OnKeyDown(key);
                }
            }
            lastPressedKeys = pressedKeys;
        }

        public void OnKeyUp(Keys key)
        {

        }
        public void OnKeyDown(Keys key)
        {
            Char bandera = (Char)key;
            if(Char.IsLetter(bandera))
            {
                if (key == Keys.Back && myName.Length > 0)
                {
                    myName = myName.Remove(myName.Length - 1);
                }
                else
                {
                    myName += key.ToString();
                }
            }
            
            
        }

        public bool CambioPantalla(bool pantalla)
        {
            if(pantalla == true)
            {
                pantalla = false;
            }
            else
            {
                pantalla = false;
            }
            return pantalla;
        }
    }
}