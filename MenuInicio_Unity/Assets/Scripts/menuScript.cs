﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using System.Collections;

public class menuScript : MonoBehaviour
{

    public Canvas quitMenu;
    public Canvas newGameMenu;
    public Canvas optionMenu;
    public Button NewGameText;
    public Button LoadGameText;
    public Button optionText;
    public Button exitText;
    public Button noAA;
    public Button AAx2;
    public Button AAx4;
    public Button AAx8;
    public Button btfullScreen;
    public Button btwindowed;
    public Dropdown dpdResolution;
    public Button bthz60;
    public Button bthz120;
    public Button btSyncYes;
    public Button btSyncNo;
    public Button btIncrease;
    public Button btDecrease;
    public int ResX;
    public int ResY;
    // Use this for initialization
    void Start()
    {
        quitMenu = quitMenu.GetComponent<Canvas>();
        newGameMenu = newGameMenu.GetComponent<Canvas>();
        optionMenu = optionMenu.GetComponent<Canvas>();
        NewGameText = NewGameText.GetComponent<Button>();
        LoadGameText = LoadGameText.GetComponent<Button>();
        exitText = exitText.GetComponent<Button>();
        optionText = optionText.GetComponent<Button>();
        noAA = noAA.GetComponent<Button>();
        AAx2 = AAx2.GetComponent<Button>();
        AAx4 = AAx4.GetComponent<Button>();
        AAx8 = AAx8.GetComponent<Button>();
        btfullScreen = btfullScreen.GetComponent<Button>();
        btwindowed = btwindowed.GetComponent<Button>();
        btIncrease = btIncrease.GetComponent<Button>();
        btDecrease = btDecrease.GetComponent<Button>();
        dpdResolution = dpdResolution.GetComponent<Dropdown>();
        quitMenu.enabled = false;
        newGameMenu.enabled = false;
        optionMenu.enabled = false;
        noAA.enabled = false;
        if(Screen.fullScreen == true)
        {
            btfullScreen.enabled = false;
        }
        else
        {
            btwindowed.enabled = false;
        }
        ResX = Screen.currentResolution.width;
        ResY = Screen.currentResolution.height;
    }

    public void ExitPress()
    {
        quitMenu.enabled = true;
        NewGameText.enabled = false;
        LoadGameText.enabled = false;
        optionText.enabled = false;
        exitText.enabled = false;
    }

    public void NewGamePress()
    {
        newGameMenu.enabled = true;
        NewGameText.enabled = false;
        LoadGameText.enabled = false;
        optionText.enabled = false;
        exitText.enabled = false;
    }

    public void NoPress()
    {
        quitMenu.enabled = false;
        NewGameText.enabled = true;
        LoadGameText.enabled = true;
        optionText.enabled = true;
        exitText.enabled = true;
    }

    public void OptionPress()
    {
        optionMenu.enabled = true;
        NewGameText.enabled = false;
        LoadGameText.enabled = false;
        optionText.enabled = false;
        exitText.enabled = false;
    }

    public void CancelPress()
    {
        newGameMenu.enabled = false;
        NewGameText.enabled = true;
        LoadGameText.enabled = true;
        optionText.enabled = true;
        exitText.enabled = true;
    }

    public void noAAPress()
    {
        QualitySettings.antiAliasing = 0;
        noAA.enabled = false;
        AAx2.enabled = true;
        AAx4.enabled = true;
        AAx8.enabled = true;
    }

    public void AAx2Press()
    {
        QualitySettings.antiAliasing = 2;
        noAA.enabled = true;
        AAx2.enabled = false;
        AAx4.enabled = true;
        AAx8.enabled = true;
    }

    public void AAx4Press()
    {
        QualitySettings.antiAliasing = 4;
        noAA.enabled = true;
        AAx2.enabled = true;
        AAx4.enabled = false;
        AAx8.enabled = true;
    }

    public void AAx8Press()
    {
        QualitySettings.antiAliasing = 8;
        noAA.enabled = true;
        AAx2.enabled = true;
        AAx4.enabled = true;
        AAx8.enabled = false;
    }

    public void FullScreenPress()

    { 
        Screen.fullScreen = !Screen.fullScreen;
        btfullScreen.enabled = false;
        btwindowed.enabled = true;
    }

    public void WindowedPress()
    {
        Screen.fullScreen = !Screen.fullScreen;
        btfullScreen.enabled = true;
        btwindowed.enabled = false;
    }

    public void Hz60Press()
    {
        Screen.SetResolution(ResX, ResY, Screen.fullScreen, 60);
        bthz60.enabled = false;
        bthz120.enabled = true;

    }

    public void Hz120Press()
    {
        Screen.SetResolution(ResX, ResY, Screen.fullScreen, 120);
        bthz60.enabled = true;
        bthz120.enabled = false;

    }



    public void StartLevel(int num)
    {
        LoadingScreenManager.LoadScene(num);
    }

    public void LoadLevel()
    {
        SceneManager.LoadScene("NombreDelNivel");
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void ExitOptionPress()
    {
        optionMenu.enabled = false;
        NewGameText.enabled = true;
        LoadGameText.enabled = true;
        optionText.enabled = true;
        exitText.enabled = true;
    }

    public void SyncYesPress()
    {
        QualitySettings.vSyncCount = 1;
        btSyncYes.enabled = false;
        btSyncNo.enabled = true;
    }

    public void SyncNoPress()
    {
        QualitySettings.vSyncCount = 0;
        btSyncYes.enabled = true;
        btSyncNo.enabled = false;
    }

    public void QualityPress()
    {
        QualitySettings.IncreaseLevel();
        btDecrease.enabled = true;
        btIncrease.enabled = false;
    }

    public void NoQualityPress()
    {
        QualitySettings.DecreaseLevel();
        btDecrease.enabled = false;
        btIncrease.enabled = true;
    }


    void Update()
    {
        if(dpdResolution.value == 0)
        {
            Screen.SetResolution(2560, 1600, Screen.fullScreen);
            ResX = 2560;
            ResY = 1600;
        }
        if (dpdResolution.value == 1)
        {
            Screen.SetResolution(1920, 1080, Screen.fullScreen);
            ResX = 1920;
            ResY = 1080;
        }
        if (dpdResolution.value == 2)
        {
            Screen.SetResolution(1366, 768, Screen.fullScreen);
            ResX = 1366;
            ResY = 768;
        }
        if (dpdResolution.value == 3)
        {
            Screen.SetResolution(1280, 720, Screen.fullScreen);
            ResX = 1280;
            ResY = 720;
        }
        if (dpdResolution.value == 4)
        {
            Screen.SetResolution(800, 600, Screen.fullScreen);
            ResX = 800;
            ResY = 600;
        }
        if (dpdResolution.value == 5)
        {
            Screen.SetResolution(640, 480, Screen.fullScreen);
            ResX = 640;
            ResY = 480;
        }
    }
}

