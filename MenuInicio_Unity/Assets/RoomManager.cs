﻿using UnityEngine;
using System.Collections;
using System;

public class RoomManager: MonoBehaviour {

    public enum TipoBloque
    {
        Destructible,Indestructible,Void
    }

    public GameObject m_TankPrefab;
    public GameObject m_Wall;
    public GameObject m_Destructable;
    public GameObject m_Indestructible;
    private TipoBloque[,] _room;
    private System.Random _aleatorio;
    private bool isPlayerSpawned = false;


    // Use this for initialization
    void Start () {

        _aleatorio = new System.Random();
        CrearRoom();
	
	}

    private void CrearRoom()
    {
        _room = new TipoBloque[_aleatorio.Next(8, 20), _aleatorio.Next(8, 30)];
        //recorrer todas las filas y columnas
        for (int i = 0; i < _room.GetLength(0); i++)
        {
            for (int j = 0; j < _room.GetLength(1); j++)
            {
                //rellena con bloques indestructible en los bordes
                _room[i, j] = (i == 0 || i == _room.GetLength(0) - 1 || j == 0 || j == _room.GetLength(1) - 1 ?
                    TipoBloque.Indestructible :
                            //Agrega bloques destruibles en las posiciones impares    
                            (i % 2 == 0) && (j % 2 == 0) ?
                            TipoBloque.Destructible :
                                //hay una probabilidad de un x% de agregar un nuevo bloque destruible entre dos destruibles
                                (_aleatorio.Next(0, 5) == 0) ?
                                TipoBloque.Destructible : TipoBloque.Void);
            }
        }


        DrawRoom();
    }

    // Update is called once per frame
    void Update () {
        if (Input.GetKey(KeyCode.Space))
        {
            CrearRoom();
        }
	
	}

    private void DrawTanque()
    {
    }

    private void DrawRoom()
    {
        for (int i = 0; i < _room.GetLength(0); i++)
        {
            for (int j = 0; j < _room.GetLength(1); j++)
            {
                m_Indestructible = Instantiate(m_Indestructible);
                float Y = m_Indestructible.transform.position.y;
                m_Indestructible.transform.position = new Vector3(i * 1f, Y, j * 1f);
                switch (_room[i,j].ToString())
                {
                    case "Destructible":
                        m_Destructable = Instantiate(m_Destructable);
                        Y = m_Destructable.transform.position.y;
                        m_Destructable.transform.position = new Vector3(i * 1f, Y, j * 1f);
                        break;
                    case "Indestructible":
                        m_Wall = Instantiate(m_Wall);
                        Y = m_Wall.transform.position.y;
                        m_Wall.transform.position = new Vector3(i * 1f, Y, j * 1f);
                        break;
                    case "Void":
                        if(!isPlayerSpawned)ColocarPlayer();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    private void ColocarPlayer()
    {
        int i = 0;
        int j = 0;
        for (i=_aleatorio.Next(_room.GetLength(0)-1); !isPlayerSpawned; i++)
        {
            for (j=_aleatorio.Next(_room.GetLength(1)-1); !isPlayerSpawned; j++)
            {
                if (_room[i,j].ToString().Equals("Void"))
                {
                    m_TankPrefab = Instantiate(m_TankPrefab);
                    float Y = m_TankPrefab.transform.position.y;
                    m_TankPrefab.transform.position = new Vector3(i * 1f, Y, j * 1f);
                    isPlayerSpawned = true;
                }
                else
                {
                    i = _aleatorio.Next(_room.GetLength(0));
                    j = _aleatorio.Next(_room.GetLength(1));
                }
            }
        }
                
    }
}
