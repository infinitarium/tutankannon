﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Menu
{
   public class Image
    {
        public float alpha;
        public string text, fontName, path;
        public Vector2 position, scale;
        public Rectangle sourceRect;
        public bool isActive;

        public Texture2D texture;
        Vector2 origin;
        ContentManager content;
        RenderTarget2D renderTarget;
        SpriteFont font;
        Dictionary<string, ImageEffect> effectList;
        public string effects;

        public FadeEffect fadeEffect;

       public void setEffect<T>(ref T effect)
        {
           if(effect == null)
           {
               effect = (T)Activator.CreateInstance(typeof(T));
           }
           else
           {
               (effect as ImageEffect).isActive = true;
               var obj = this;
               (effect as ImageEffect).LoadContent(ref obj);
           }

           effectList.Add(effect.GetType().ToString().Replace("Menu", ""), (effect as ImageEffect));
        }

       public void ActiveEffect(string effect)
       {
           if(effectList.ContainsKey(effect))
           {
               effectList[effect].isActive = true;
               var obj = this;
               effectList[effect].LoadContent(ref obj);
           }
       }

       public void DesactiveEffect(string effect)
       {
           if (effectList.ContainsKey(effect))
           {
               effectList[effect].isActive = false;
               effectList[effect].UnloadContent();
           }
       }

        public Image()
        {
            path = string.Empty;
            text = string.Empty;
            effects = string.Empty;
            fontName = "Fonts/Orbitron";
            scale = Vector2.One;
            alpha = 1.0f;
            sourceRect = Rectangle.Empty;
            effectList = new Dictionary<string,ImageEffect>();
        }

        public void LoadContent()
        {
            content = new ContentManager(
                ScreenManager.Instance.Content.ServiceProvider, "Content");

            if(path != string.Empty)
            {
                texture = content.Load<Texture2D>(path);
            }

            Vector2 dimesion = Vector2.Zero;
            font = content.Load<SpriteFont>(fontName);

            if(texture != null)
            {
                dimesion.X += texture.Width;
                dimesion.X += font.MeasureString(text).X;
                dimesion.Y = Math.Max(texture.Height, font.MeasureString(text).Y);
            }else{
                dimesion.Y = font.MeasureString(text).Y;
            }

            if(sourceRect == Rectangle.Empty)
            {
                sourceRect = new Rectangle(0, 0, (int)dimesion.X, (int)dimesion.Y);
            }

            renderTarget = new RenderTarget2D(ScreenManager.Instance.graphicsDevice,(int)dimesion.X , (int)dimesion.Y);
            ScreenManager.Instance.graphicsDevice.SetRenderTarget(renderTarget);
            ScreenManager.Instance.graphicsDevice.Clear(Color.Transparent);
            ScreenManager.Instance.spriteBatch.Begin();
            if(texture != null)
                ScreenManager.Instance.spriteBatch.Draw(texture, Vector2.Zero, Color.White);
            ScreenManager.Instance.spriteBatch.DrawString(font, text, Vector2.Zero, Color.White);
            ScreenManager.Instance.spriteBatch.End();

            texture = renderTarget;
            ScreenManager.Instance.graphicsDevice.SetRenderTarget(null);

            setEffect<FadeEffect>(ref fadeEffect);

            if(effects != string.Empty)
            {
                string[] split = effects.Split(':');
                foreach (string item in split)
                {
                    ActiveEffect(item);
                }
            }
        }

        public void UnloadContent()
        {
            content.Unload();
            foreach (var effect in effectList)
            {
                DesactiveEffect(effect.Key);
            }
        }

        public void Update(GameTime gameTime)
        {
             foreach (var effect in effectList)
             {
                 if (effect.Value.isActive)
                 {
                     effect.Value.Update(gameTime);
                 }
             }
            
        }

        public void Draw(SpriteBatch sprintBatch)
        {
            origin = new Vector2(sourceRect.Width / 2, sourceRect.Height / 2);
            sprintBatch.Draw(texture, position + origin, sourceRect, Color.White, 0.0f, origin, scale, SpriteEffects.None, 0.0f);
        }
    }
   
   
}
