﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Menu
{
    public class ScreenManager
    {
        private static ScreenManager instance;
        public Vector2 Dimension { private set; get; }
        public ContentManager Content { private set; get; }
        XmlManager<GameScreen> xmlGameScreenManager;

        GameScreen currentScreen, newScreen;
        public GraphicsDevice graphicsDevice;
        public SpriteBatch spriteBatch;
        public Image image;
        public static ScreenManager Instance
        {
            get
            {
                if(instance == null)
                {
                    XmlManager<ScreenManager> xml = new XmlManager<ScreenManager>();
                    instance = xml.Load("Load/ScreenManager.xml");
                    instance = new ScreenManager();
                }
                return instance;
            }
        }

        public void ChangeScreen(string screenName)
        {
            newScreen = (GameScreen)Activator.CreateInstance(Type.GetType("Menu." + screenName));
        }

        public void Trasition()
        {

        }

        private ScreenManager()
        {
            Dimension = new Vector2(640, 480);
            currentScreen = new SplashScreen();
            xmlGameScreenManager = new XmlManager<GameScreen>();
            xmlGameScreenManager.type = currentScreen.type;
            currentScreen = xmlGameScreenManager.Load("C:/Users/JavierJerez/Documents/tutankannon/MenuEjemplo/Menu/Content/Load/SplashScreen.xml");
        }

        public void LoadContent(ContentManager Content)
        {
            this.Content = new ContentManager(Content.ServiceProvider, "Content");
            currentScreen.LoadContent();
        }

         public void UnloadContent()
        {
            currentScreen.UnloadContent();
        }

        public void Update(GameTime gametime)
        {
            currentScreen.Update(gametime);
        }

        public void Draw(SpriteBatch sprintBatch)
        {
            currentScreen.Draw(sprintBatch);
        }
    }
}
