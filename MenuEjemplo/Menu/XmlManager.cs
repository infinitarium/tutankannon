﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;
using System.Xml.Serialization;

namespace Menu
{
    public class XmlManager<T>
    {
        public Type type;
        public T Load(string path)
        {
            T instance;
            using (TextReader reader = new StreamReader(path))
            {             
                XmlSerializer xml = new XmlSerializer(typeof(Type));
                instance = (T)xml.Deserialize(reader);
            }
            return instance;
        }
        public void Save(string path, object obj)
        {
            using (TextWriter writer = new StreamWriter(path))
            {
                XmlSerializer xml = new XmlSerializer(type);
                xml.Serialize(writer, obj);
            }
        }
    }
}
