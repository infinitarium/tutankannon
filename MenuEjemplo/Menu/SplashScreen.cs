﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Menu
{
    public class SplashScreen : GameScreen
    {
        public Image image;

        public override void LoadContent()
        {
            base.LoadContent();
            image.LoadContent();
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
            image.UnloadContent();
        }

        public override void Update(GameTime gametime)
        {
            base.Update(gametime);
            image.Update(gametime);
        }

        public override void Draw(SpriteBatch sprintBatch)
        {

            image.Draw(sprintBatch);
        }

    }
}
